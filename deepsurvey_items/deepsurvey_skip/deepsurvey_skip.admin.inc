<?php

/**
 * @file
 * Administrator interface for Deep Survey Skip module.
 *
 * Admin functions.
 *
 * Admin page related function used purely by the deep survey skip module.
 */

// DEEP SURVEY SKIP ADMIN.
/**
 * Calls the skip edit form.
 *
 * Displays information about a survey skip node and allows items to be added
 * to it.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the skip node.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Formatted age displaying the items in the question and with controls for
 *   doing various things associated with each.
 */
function deepsurvey_skip_edit_survey($deepsurvey_item_nid, $current_path, $last_page) {
  // Add code to determine if the node is a survey related one or not - go to
  // the node edit page if it is not a survey - add new db table to store this
  // info.
  $node_properties = db_fetch_object(db_query('
    SELECT type, title FROM {node}
    WHERE nid = %d', $deepsurvey_item_nid));

  if ($node_properties->type == 'deepsurvey_skip') {
    drupal_set_title($node_properties->title);
    $rc = drupal_get_form('deepsurvey_edit_form', $deepsurvey_item_nid,
      $node_properties->type, $current_path, $last_page);

    $deepsurvey_title = db_fetch_object(db_query('
      SELECT title FROM {node}
      WHERE nid = %d', $deepsurvey_item_nid));

  }
  else {
    $rc = t('<p>test</p>');
  }

  return $rc;
}


/**
 * Add new rules to a survey skip.
 *
 * Calls the form function to add new rules to the skip.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the skip
 *
 * @param int $order
 *   The order of the item before where the existing answers are to be added
 *   into the skip, 0 if they are to be added to the beginning of the survey.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form.
 */
function deepsurvey_skip_edit_add($deepsurvey_item_nid, $order, $current_path, $last_page) {
  $rc = drupal_get_form('deepsurvey_skip_add_new_form', $deepsurvey_item_nid,
    $order, $current_path, $last_page);

  return $rc;
}

// THEME FUNCTIONS.
// Remember to updated deepsurvey_theme() in survey.module.
/**
 * Theme the form for adding questions to a survey.
 */
function theme_deepsurvey_skip_add_new_skips_table($form) {

  $headers = array(
    t('Skip When Item'),
    t('Operation'),
    t('Row Code'),
    t('Column Code'),
    t('Drop Down Code'),
    t('Text/Numeric Response'),
    t('Skip to Item'),
  );
  $rows = array();

  if (!empty($form)) {
    foreach (element_children($form) as $nid) {

      $rows[] = array(
        'data' => array(
          drupal_render($form[$nid]['response title']),
          drupal_render($form[$nid]['operation']),
          drupal_render($form[$nid]['row code']),
          drupal_render($form[$nid]['col code']),
          drupal_render($form[$nid]['drop down']),
          drupal_render($form[$nid]['response']),
          drupal_render($form[$nid]['target title']),
        ),
      );
    }

  }
  $table = theme('table', $headers, $rows);
  return $table . drupal_render($form);
}

/**
 * Theme the form for editing the survey question.
 */
function theme_deepsurvey_skip_edit_table($form) {

  $headers = array(
    t('Order'),
    t('Skip When Item'),
    t('Operation'),
    t('Row Code'),
    t('Column Code'),
    t('Drop Down Code'),
    t('Text/Numeric Response'),
    t('Skip to Item'),
    t('Reorder'),
    t('Remove'),
    t('Add new'),
  );
  $rows = array();

  if (!empty($form)) {
    foreach (element_children($form) as $nid) {

      $rows[] = array(
        'data' => array(
          check_plain(drupal_render($form[$nid]['order'])),
          drupal_render($form[$nid]['response title']),
          drupal_render($form[$nid]['operation']),
          drupal_render($form[$nid]['row code']),
          drupal_render($form[$nid]['col code']),
          drupal_render($form[$nid]['drop down']),
          drupal_render($form[$nid]['response']),
          drupal_render($form[$nid]['target title']),
          drupal_render($form[$nid]['reorder box']),
          drupal_render($form[$nid]['remove box']),
          drupal_render($form[$nid]['add new']),
        ),
      );
    }

  }
  $table = theme('table', $headers, $rows);
  return $table . drupal_render($form);
}
