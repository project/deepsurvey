<?php

/**
 * @file
 * Skip through survey.
 *
 * Skip function.
 *
 * Contains a function which allows skipping to a question.
 */

/**
 * Implementation of functions for skipping through the survey.
 *
 * Called from deepsurvey_view (or other container module).
 *
 * @param int $instance
 *   Question instance.
 *
 * @param int $survey
 *   The node id of the survey.
 *
 * @param int $usr
 *   The user id of the user viewing the survey.
 *
 * @param int $parent_item
 *   The node id of the container type which the question is in (this will not
 *   always be the survey - could be a loop or section).
 *
 * @param string $parent_type
 *   The type of container the question is in (this will not always be the
 *   survey - could be a loop or section).
 *
 * @param sting $loops
 *   The current loop value - this will be build the loop type and inserted
 *   into the data table as is.
 *
 * @param array $state_values
 *   The current state values - these are the state valiables of the current
 *   container.
 *
 * @param array $state_values_global
 *   The current global state values - these are the state variables of the
 *   survey.
 *
 * @return array
 *   Returns either the next question form or target question form.
 */
function deepsurvey_skip_deepsurvey_item_display($instance, $survey, $usr, $parent_item, $parent_type, $loops, &$state_values, &$state_values_global) {
  $rc = '';
  $rule_found = FALSE;

  // Check for filtering.
  $question_filter = db_fetch_object(db_query('
    SELECT * FROM {deepsurvey_node_questionmask}
    WHERE userid=%d
    AND deepsurvey_nid=%d
    AND instance = %d
    AND masked_question = %d',
    $usr, $survey,
    $state_values_global->current_questionmask_instance,
    $state_values->current_question));

  if (!$question_filter->deepsurvey_nid && $state_values_global->current_direction) {
    // Loop through rules and implement first valid rule.
    $check_standard = NULL;
    $check_numeric = NULL;
    $check_text = NULL;
    $check_all = NULL;

    $rules = db_query('
      SELECT * FROM {deepsurvey_skip_responses}
      WHERE deepsurvey_nid = %d', $state_values->current_question);
    while ($current_rule = db_fetch_object($rules)) {
      // Check questions.
      $operation_substitute = '=';
      switch ($current_rule->operation) {
        case DEEPSURVEY_SKIP_LT:
          $operation_substitute = '<';
          break;

        case DEEPSURVEY_SKIP_EQ:
          $operation_substitute = '=';
          break;

        case DEEPSURVEY_SKIP_GT:
          $operation_substitute = '>';
          break;

        default:
          $operation_substitute = '=';
      }

      if (!is_null($current_rule->response_nid)) {
        if (is_null($current_rule->drop_down_code) && is_null($current_rule->column_code)) {
          $check_standard = db_fetch_object(db_query('
            SELECT response_id FROM {deepsurvey_node_data_standard}
            WHERE userid = %d AND deepsurvey_nid = %d
            AND question_nid = %d AND row_code %s %d',
            $usr, $survey, $current_rule->response_nid,
            $operation_substitute, $current_rule->row_code)
          );
          if ($current_rule->response == '') {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND row_code %s %d',
              $usr, $survey, $instance, $loops,
              $current_rule->response_nid, $operation_substitute,
              $current_rule->row_code)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND row_code %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $operation_substitute, $current_rule->row_code)
            );
          }
          else {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND row_code = %d
              AND response %s %d',
              $usr, $survey, $instance, $loops,
              $current_rule->response_nid, $current_rule->row_code,
              $operation_substitute, $current_rule->response)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND row_code = %d
              AND response %s "%s"',
              $usr, $survey, $instance, $loops,
              $current_rule->response_nid, $current_rule->row_code,
              $operation_substitute, $current_rule->response)
            );
          }
        }
        elseif (is_null($current_rule->drop_down_code) && is_null($current_rule->row_code)) {
          $check_standard = db_fetch_object(db_query('
            SELECT response_id FROM {deepsurvey_node_data_standard}
            WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
            AND loops = "%s" AND question_nid = %d AND column_code %s %d',
            $usr, $survey, $instance, $loops, $current_rule->response_nid,
            $operation_substitute, $current_rule->column_code)
          );
          if ($current_rule->response == '') {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $operation_substitute, $current_rule->column_code)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $operation_substitute, $current_rule->column_code)
            );
          }
          else {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code = %d
              AND response %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->column_code, $operation_substitute,
              $current_rule->response)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code = %d
              AND response %s "%s"',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->column_code, $operation_substitute,
              $current_rule->response)
            );
          }
        }
        elseif (is_null($current_rule->column_code) && is_null($current_rule->row_code)) {
          $check_standard = db_fetch_object(db_query('
            SELECT response_id FROM {deepsurvey_node_data_standard}
            WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
            AND loops = "%s" AND question_nid = %d AND response %s %d',
            $usr, $survey, $instance, $loops, $current_rule->response_nid,
            $operation_substitute, $current_rule->drop_down_code)
          );
          if ($current_rule->response != '') {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND response %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $operation_substitute, $current_rule->response)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND response %s "%s"',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $operation_substitute, $current_rule->response)
            );
          }
        }
        elseif (is_null($current_rule->column_code)) {
          $check_standard = db_fetch_object(db_query('
            SELECT response_id FROM {deepsurvey_node_data_standard}
            WHERE userid = %d AND deepsurvey_nid = %d
            AND instance = %d AND loops = "%s" AND question_nid = %d
            AND row_code = %d AND response %s %d',
            $usr, $survey, $instance, $loops, $current_rule->response_nid,
            $current_rule->row_code, $operation_substitute,
            $current_rule->drop_down_code)
          );
          if ($current_rule->response != '') {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND row_code = %d
              AND response %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->row_code, $operation_substitute,
              $current_rule->response)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND row_code = %d
              AND response %s "%s"',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->row_code, $operation_substitute,
              $current_rule->response)
            );
          }
        }
        elseif (is_null($current_rule->row_code)) {
          $check_standard = db_fetch_object(db_query('
            SELECT response_id FROM {deepsurvey_node_data_standard}
            WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
            AND loops = "%s" AND question_nid = %d AND column_code = %d
            AND response %s %d',
            $usr, $survey, $instance, $loops, $current_rule->response_nid,
            $current_rule->column_code, $operation_substitute,
            $current_rule->drop_down_code)
          );
          if ($current_rule->response != '') {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code = %d
              AND response %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->column_code, $operation_substitute,
              $current_rule->response)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code = %d
              AND response %s "%s"', $usr, $survey, $instance, $loops,
              $current_rule->response_nid, $current_rule->column_code,
              $operation_substitute, $current_rule->response)
            );
          }
        }
        elseif (is_null($current_rule->drop_down_code)) {
          $check_standard = db_fetch_object(db_query('
            SELECT response_id FROM {deepsurvey_node_data_standard}
            WHERE userid = %d AND deepsurvey_nid = %d
            AND instance = %d AND loops = "%s" AND question_nid = %d
            AND row_code = %d AND column_code %s %d',
            $usr, $survey, $instance, $loops, $current_rule->response_nid,
            $current_rule->row_code, $operation_substitute,
            $current_rule->column_code)
          );
          if ($current_rule->response == '') {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND row_code = %d
              AND column_code %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->row_code, $operation_substitute,
              $current_rule->column_code)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND row_code = %d
              AND column_code %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->row_code, $operation_substitute,
              $current_rule->column_code)
            );
          }
          else {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code = %d
              AND row_code = %d AND response %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->column_code, $current_rule->row_code,
              $operation_substitute, $current_rule->response)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code = %d
              AND row_code = %d AND response %s "%s"',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->column_code, $current_rule->row_code,
              $operation_substitute, $current_rule->response)
            );
          }
        }
        elseif (is_null($current_rule->column_code) && is_null($current_rule->row_code) && is_null($current_rule->drop_down)) {
          if ($current_rule->response == '') {
            $check_all = TRUE;
          }
          else {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND response %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $operation_substitute, $current_rule->response)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND response %s "%s"',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $operation_substitute, $current_rule->response)
            );
          }
        }
        else {
          $check_standard = db_fetch_object(db_query('
            SELECT response_id FROM {deepsurvey_node_data_standard}
            WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
            AND loops = "%s" AND question_nid = %d AND column_code = %d
            AND row_code = %d AND response %s %d',
            $usr, $survey, $instance, $loops, $current_rule->response_nid,
            $current_rule->column_code, $current_rule->row_code,
            $operation_substitute, $current_rule->drop_down_code)
          );
          if ($current_rule->response != '') {
            $check_numeric = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_numeric}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code = %d
              AND row_code = %d AND response %s %d',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->column_code, $current_rule->row_code,
              $operation_substitute, $current_rule->response)
            );
            $check_text = db_fetch_object(db_query('
              SELECT response_id FROM {deepsurvey_node_data_text}
              WHERE userid = %d AND deepsurvey_nid = %d AND instance = %d
              AND loops = "%s" AND question_nid = %d AND column_code = %d
              AND row_code = %d AND response %s "%s"',
              $usr, $survey, $instance, $loops, $current_rule->response_nid,
              $current_rule->column_code, $current_rule->row_code,
              $operation_substitute, $current_rule->response)
            );
          }
        }
      }

      // Check for a match.
      if ((is_null($current_rule->response_nid) || $check_all || !is_null($check_standard->response_id) || !is_null($check_numeric->response_id) || !is_null($check_text->response_id))
          && !is_null($current_rule->target_nid)) {
        // Change current question and call function for that question.
        // Check that the target exists.
        $target_check = NULL;
        $target_check = db_fetch_object(db_query('
          SELECT item_nid FROM {%s_node_relationship}
          WHERE item_nid = %d AND deepsurvey_nid = %d',
          $parent_type, $current_rule->target_nid, $parent_item));

        if (!is_null($target_check->item_nid)) {
          db_query('
            UPDATE {%s_node_state}
            SET current_question = %d ,current_item_instance = %d
            WHERE userid = %d AND deepsurvey_nid = %d',
            $parent_type, $current_rule->target_nid,
            $instance, $usr, $parent_item);
          $rc = '';
        }
        elseif ($parent_type != 'deepsurvey') {
          $rc = ':#forward ' . $current_rule->target_nid;
        }
        else {
          if (!_deepsurvey_next_item($parent_item, $parent_type, $state_values->current_question, $state_values->current_item_instance, $usr)) {
            $rc = ':#end';
          }
        }
        $rules = NULL;
        $rule_found = TRUE;
      }
    }
    if (!$rule_found) {
      if (!_deepsurvey_next_item($parent_item, $parent_type, $state_values->current_question, $state_values->current_item_instance, $usr)) {
        $rc = ':#end';
      }
    }
  }
  else {
    if ($state_values_global->current_direction) {
      if (!_deepsurvey_next_item($parent_item, $parent_type, $state_values->current_question, $state_values->current_item_instance, $usr)) {
        $rc = ':#end';
      }
    }
    else {
      if (!_deepsurvey_previous_item($survey, $parent_item, $parent_type, $current_question, $state_values->current_item_instance, $usr, $state_values_global->current_mask_instance, $state_values_global->current_scalemask_instance, $state_values_global->current_questionmask_instance, $state_values_global->current_substitution_instance)) {
        $rc = ':#begin';
      }
    }

  }

  return $rc;
}
