<?php

/**
 * @file
 * Administrator form functions for Deep Survey Answer List module.
 *
 * Admin form functions.
 *
 * Contains all the functionality of the forms from the admin pages that need
 * global scope.
 */

/**
 * Ceates the deepsurvey answer lists' edit form.
 *
 * This is the survey answer lists' implementation of the node specific edit
 * function.
 *
 * @param int $deepsurvey_item_nid
 *   Node id of the item displaying it's child items.
 *
 * @param string $node_type
 *   The type of the item displaying it's child items.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form.
 */
function deepsurvey_answer_list_deepsurvey_edit_form($deepsurvey_item_nid, $node_type, $current_path, $last_page) {

  // Counts the loops and hence the order of the items.
  $order = 0;

  $form['start'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );
  $form['start']['new'] = array(
    '#value' => l(t('add new answers to the start of the answer list'),
      $current_path . '/answer_list/0'),
    '#theme' => 'item',
  );
  $form['table'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#theme' => 'deepsurvey_answer_list_edit_table',
  );

  $result = db_query('
    SELECT item_order, response, code FROM {deepsurvey_answer_list_responses}
    WHERE deepsurvey_nid = %d ORDER BY item_order', $deepsurvey_item_nid);
  while ($nodes = db_fetch_object($result)) {
    $order++;

    $form['table'][$nodes->item_order]['order'] = array(
      '#value' => $order,
    );
    $form['table'][$nodes->item_order]['answer text'] = array(
      '#type' => 'textfield',
      '#size' => 20,
      '#default_value' => $nodes->response,
      '#parents' => array('table', t($nodes->item_order), 'answer text'),
    );
    $form['table'][$nodes->item_order]['code'] = array(
      '#type' => 'textfield',
      '#size' => 4,
      '#maxlength' => 6,
      '#default_value' => $nodes->code,
      '#parents' => array('table', t($nodes->item_order), 'code'),
    );
    $form['table'][$nodes->item_order]['reorder box'] = array(
      '#type' => 'textfield',
      '#size' => 4,
      '#maxlength' => 4,
      '#parents' => array('table', t($nodes->item_order), 'reorder box'),

    );
    $form['table'][$nodes->item_order]['remove box'] = array(
      '#type' => 'checkbox',
      '#parents' => array('table', t($nodes->item_order), 'remove box'),
    );
    $form['table'][$nodes->item_order]['add new'] = array(
      '#value' => l(t('add new answers here'),
      $current_path . '/answer_list/' . $nodes->item_order),
    );
  };

  $form['last page'] = array(
    '#type' => 'value',
    '#value' => $last_page,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Done'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
  );

  return $form;
}

/**
 * Submit function for survey question edit form.
 *
 * @param object $form
 *   Standard form submit function parameter.
 *
 * @param object $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_answer_list_deepsurvey_edit_form_submit($form, $form_state) {
  deepsurvey_edit_process_answer_list($form, $form_state,
   'deepsurvey_answer_list_responses');
}


/**
 * Validate function for survey question edit form.
 *
 * @param object $form
 *   Standard form submit function parameter.
 *
 * @param object $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_answer_list_deepsurvey_edit_form_validate($form, &$form_state) {
  deepsurvey_edit_validate_answer_list($form, $form_state);
}

/**
 * Processes the edit form.
 *
 * This is the main function where the processing of the edit form takes place,
 * it has been disentangled from the node specific function so other deep
 * survey modules can use it.
 *
 * @param object $form
 *   Standard form submit function parameter.
 *
 * @param object $form_state
 *   Standard form submit function parameter.
 *
 * @param string $node_table_properties
 *   String containing the name of the particular node properties table for
 *   this type.
 */
function deepsurvey_edit_process_answer_list($form, &$form_state, $node_table_properties) {

  if ($form_state['clicked_button']['#value'] == t('Done')) {
    // Flag any reorder boxes being set.
    $local_flag = FALSE;
    $reset_flag = FALSE;
    $local_order = 1;
    $local_prev = 0;
    $local_offset = 0;
    $new_id = 0;
    // Item id of the reordered items.
    $new_order = array();
    // Relationship id of the items not being reordered.
    $old_order = array();
    reset($form_state['values']['table']);

    while (list($key, $form_order) = each($form_state['values']['table'])) {
      if ($form_state['values']['table'][$key]['remove box'] == TRUE) {
        db_query('
          DELETE FROM {deepsurvey_answer_list_responses}
          WHERE item_order = %d AND deepsurvey_nid = %d',
          $key, $form_state['values']['nid']);
      }
      elseif ($form_state['values']['table'][$key]['reorder box'] != '') {
        // Remove item from current order.
        db_query('
          DELETE FROM {deepsurvey_answer_list_responses}
          WHERE item_order = %d AND deepsurvey_nid = %d',
          $key, $form_state['values']['nid']);
        $new_order[$form_state['values']['table'][$key]['reorder box']] = array(
          $form_state['values']['table'][$key]['answer text'],
          $form_state['values']['table'][$key]['code'],
        );
        $local_flag = TRUE;
      }
      else {
        $old_order[$local_order] = $key;
        $local_order++;
        // Just update everything for now, optimise later if need be.
        db_query('
          UPDATE {deepsurvey_answer_list_responses} SET response = "%s", code = "%s"
          WHERE item_order = %d AND deepsurvey_nid=%d',
          $form_state['values']['table'][$key]['answer text'],
          $form_state['values']['table'][$key]['code'],
          $key, $form_state['values']['nid']);
      }
    }

    // We now have an array of the survey item order, one with the order of the
    // items whose order was not changed and one with the changed order items.
    // Now compare the two and make changes to the database where necessary.
    $local_order = 1;
    if ($local_flag) {
      while ($old_order[$local_order - $local_offset] != FALSE || $new_order[$local_order] != FALSE) {
        if ($old_order[$local_order - $local_offset] != FALSE && $new_order[$local_order] != FALSE) {
          if ($local_prev >= $old_order[$local_order - $local_offset] + 1) {
            $loop_prev = $local_prev;
            $looper = 0;
            while ($loop_prev >= $old_order[$local_order - $local_offset + $looper] + 1) {
              db_query('
                UPDATE {deepsurvey_answer_list_responses} SET item_order = %d
                WHERE item_order = %d AND deepsurvey_nid = %d',
                $old_order[$local_order - $local_offset + $looper],
                $old_order[$local_order - $local_offset + $looper] + 1,
                $form_state['values']['nid']);
              $old_order[$local_order - $local_offset + $looper]++;
              $loop_prev = $old_order[$local_order - $local_offset + $looper];
              $looper++;
              $reset_flag = TRUE;
            }
          }
          $new_id = floor($local_prev + (($old_order[$local_order -
            $local_offset] - $local_prev) / 2));
          db_query('
            INSERT INTO {deepsurvey_answer_list_responses}
            (deepsurvey_nid, item_order, response, code)
            VALUES (%d, %d, "%s", "%s")',
            $form_state['values']['nid'], $new_id,
            $new_order[$local_order][0], $new_order[$local_order][1]);
          $local_offset++;
          $local_prev = $new_id;
        }
        elseif ($old_order[$local_order - $local_offset] == FALSE && $new_order[$local_order] != FALSE) {
          if ($local_prev < DEEPSURVEY_MAX_ITEMS) {
            $new_id = floor($local_prev + ((DEEPSURVEY_MAX_ITEMS - $local_prev) / 2));
            db_query('
              INSERT INTO {deepsurvey_answer_list_responses}
              (deepsurvey_nid, item_order, response, code)
              VALUES (%d, %d, "%s", "%s")',
              $form_state['values']['nid'], $new_id,
              $new_order[$local_order][0], $new_order[$local_order][1]);
            $local_prev = $new_id;
          }
          else {
            $reset_flag = TRUE;
          }
        }
        else {
          $local_prev = $old_order[$local_order - $local_offset];
        }
        $local_order++;
      }

      if ($reset_flag) {
        _deepsurvey_reset_order('deepsurvey_answer_list_responses',
          $form_state['values']['nid']);
      }

    }
    drupal_set_message(t('Answer List Updated'));
  }
  else {
    $form_state['redirect'] = $form_state['values']['last page'];
  }
}

/**
 * Validation function for the above.
 *
 * @param object $form
 *   Standard form submit function parameter.
 *
 * @param object $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_edit_validate_answer_list($form, &$form_state) {

  if ($form_state['clicked_button']['#value'] == t('Done')) {
    $number_of_items = 0;
    $highest_reorder = 0;
    // Item id of the reordered items.
    $reorder = array();
    reset($form_state['values']['table']);

    while (list($key, $form_order) = each($form_state['values']['table'])) {

      if (drupal_strlen($form_state['values']['table'][$key]['answer text']) > 65535) {
        form_set_error('table][' . $key . '][answer text',
          t('An answer is too long - it must be under 65535 letters and spaces'));
      }

      if (!is_numeric($form_state['values']['table'][$key]['code'])) {
        form_set_error('table][' . $key . '][code',
          t('Please enter a numeric code for each answer'));
      }

      if ($form_state['values']['table'][$key]['remove box'] == TRUE && $form_state['values']['table'][$key]['reorder box'] != '') {
        form_set_error('table][' . $key . '][remove box',
          t('You can not reorder an item marked for deletion'));
      }

      if ($form_state['values']['table'][$key]['reorder box'] < 0 || $form_state['values']['table'][$key]['reorder box'] === 0) {
        form_set_error('table][' . $key . '][reorder box',
          t('reorder value 0 or less'));
      }
      elseif ($form_state['values']['table'][$key]['reorder box'] != '') {
        if ($reorder[$form_state['values']['table'][$key]['reorder box']] > 0) {
          form_set_error('table][' . $key . '][reorder box',
          t('duplicate reorder value'));
        }
        else {
          $reorder[$form_state['values']['table'][$key]['reorder box']] = $key;
        }
      }

      if ($form_state['values']['table'][$key]['reorder box'] > $highest_reorder) {
        $highest_reorder = $form_state['values']['table'][$key]['reorder box'];
      }

      $number_of_items++;
    }

    if ($highest_reorder > $number_of_items) {
      form_set_error('table][' . $reorder[$highest_reorder] . '][reorder box',
        t('reorder value higher than the number of items'));
    }

  }
}


/**
 * Build the form that allows the user to add existing nodes to a survey.
 *
 * @param array $form_state
 *   The form state array.
 *
 * @param int $deepsurvey_item_nid
 *   If $order is 0 then this is the node id of the survey object the node(s) is
 *   being added to, otherwise it is the item/survey relationship that the nodes
 *   are to be inserted after.
 *
 * @param int $order
 *   The order of the node before the new node(s) or the value of the next
 *   relationship id if order isn't 0.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form.
 */
function deepsurvey_answer_list_add_new_form($form_state, $deepsurvey_item_nid, $order, $current_path, $last_page) {

  $deepsurvey_title = db_fetch_object(db_query('
    SELECT title FROM {node}
    WHERE nid = %d', $deepsurvey_item_nid));

  drupal_set_title(t("Add existing items to @name",
    array('@name' => check_plain($deepsurvey_title->title))));

  // Store parameters we are going to need when processing the form.
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $deepsurvey_item_nid,
  );
  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );

  $form['table'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#theme' => 'deepsurvey_answer_list_add_new_answers_table',
  );

  for ($response_loop = 1; $response_loop < 20; $response_loop++) {
    $form['table'][$response_loop]['answer'] = array(
      '#type' => 'textfield',
      '#size' => 20,
    );
    $form['table'][$response_loop]['code'] = array(
      '#type' => 'textfield',
      '#size' => 4,
      '#maxlength' => 6,
    );
  }

  $form['last page'] = array(
    '#type' => 'value',
    '#value' => $last_page . '/' . $deepsurvey_item_nid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Done'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Deepsurvey_answer_list_add_existing submit function.
 *
 * @param object $form
 *   Standard form submit function parameter.
 *
 * @param object $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_answer_list_add_new_form_submit($form, &$form_state) {

  if ($form_state['clicked_button']['#value'] == t('Done')) {
    $local_last = $form_state['values']['order'];
    $next_item_in_order = db_fetch_object(db_query('
      SELECT item_order FROM {deepsurvey_answer_list_responses}
      WHERE deepsurvey_nid = %d AND item_order>%d
      ORDER BY item_order',
      $form_state['values']['rid'], $form_state['values']['order']));
    $local_next = $next_item_in_order->item_order;
    $reset_flag = FALSE;

    // In case survey has no items.
    if (!$local_next) {
      $local_next = DEEPSURVEY_MAX_ITEMS;
    }

    foreach (element_children($form_state['values']['table']) as $nid) {
      if ($form_state['values']['table'][$nid]['code'] != '') {
        if ($local_last >= $local_next + 1) {
          $loop_prev = $local_last;
          $loop_next = $local_next;
          $local_next++;
          while ($loop_prev >= $loop_next + 1) {
            db_query('
              UPDATE {deepsurvey_answer_list_responses} SET item_order = %d
              WHERE item_order = %d AND deepsurvey_nid = %d',
              $loop_next, $loop_next + 1, $form_state['values']['rid']);
            $loop_prev = $loop_next + 1;
            $loop_next = db_fetch_object(db_query('
              SELECT item_order FROM {deepsurvey_answer_list_responses}
              WHERE deepsurvey_nid = %d AND item_order>%d
              ORDER BY item_order',
              $form_state['values']['rid'], $loop_prev));
            $reset_flag = TRUE;
          }
        }
        $new_id = floor($local_last + (($local_next - $local_last) / 2));
        db_query('
          INSERT INTO {deepsurvey_answer_list_responses}
          (deepsurvey_nid, item_order, response, code)
          VALUES (%d, %d, "%s", "%s")',
          $form_state['values']['rid'], $new_id,
          $form_state['values']['table'][$nid]['answer'],
          $form_state['values']['table'][$nid]['code']);
        $local_last = $new_id;
      }
    }

    if ($reset_flag) {
      _deepsurvey_reset_order('deepsurvey_answer_list_responses',
        $form_state['values']['nid']);
    }

    drupal_set_message(t('New responses have been added to the answer list'));
  }
  $form_state['redirect'] = $form_state['values']['last page'];
}

/**
 * Deepsurvey_answer_list_add_existing validate function.
 *
 * @param object $form
 *   Standard form submit function parameter.
 *
 * @param object $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_answer_list_add_new_form_validate($form, &$form_state) {

  if ($form_state['clicked_button']['#value'] == t('Done')) {
    reset($form_state['values']['table']);

    foreach (element_children($form_state['values']['table']) as $nid) {

      if (drupal_strlen($form_state['values']['table'][$nid]['answer']) > 65535) {
        form_set_error('table][' . $nid . '][answer',
          t('An answer is too long - must be under 65535 letters and spaces'));
      }

      if ($form_state['values']['table'][$nid]['answer'] != '' && !is_numeric($form_state['values']['table'][$nid]['code'])) {
        form_set_error('table][' . $nid . '][code',
          t('Please enter a numeric code for each answer'));
      }
    }

  }
}
