<?php

/**
 * @file
 * Administrator form functions for Deep Survey Question module.
 *
 * Admin form functions.
 *
 * Contains all the functionality of the forms from the admin pages that need
 * global scope.
 */

/**
 * Builds the form for editing a questions' answer lists.
 *
 * This is the deep survey questions implementation of the node specific edit
 * function.
 *
 * @param int $deepsurvey_item_nid
 *   Node id of the item being processed.
 *
 * @param string $node_type
 *   The type of the item being processed.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form.
 */
function deepsurvey_question_deepsurvey_edit_form($deepsurvey_item_nid, $node_type, $current_path, $last_page) {

  // Counts the loops and hence the order of the items.
  $order = 0;
  // Holds the codes values - used to check for duplicate codes in the answer
  // lists.
  $codes = array();

  $form['start'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );
  $form['start']['new'] = array(
    '#value' => l(t('add a new answer list to the start of the question'),
    $current_path . '/new/0'),
    '#theme' => 'item',
  );
  $form['start']['existing'] = array(
    '#value' => l(t('add existing answer lists to the start of the question'),
    $current_path . '/existing/0'),
    '#theme' => 'item',
  );
  $form['table'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#theme' => 'deepsurvey_question_edit_table',
  );

  $result = db_query(db_rewrite_sql('
    SELECT r.item_order, r.item_nid, r.axis, r.display_order,
    r.unique_question, r.response_type, n.title
    FROM {deepsurvey_question_node_relationship} r
    INNER JOIN {node} n ON r.item_nid = n.nid
    WHERE r.deepsurvey_nid = %d
    ORDER BY r.item_order'), $deepsurvey_item_nid);
  while ($nodes = db_fetch_object($result)) {
    $order++;

    // Get codes.
    $code_holder = db_query('
      SELECT code FROM {deepsurvey_answer_list_responses}
      WHERE deepsurvey_nid = %d', $nodes->item_nid);
    while ($code_temp = db_fetch_object($code_holder)) {
      $codes[$nodes->axis][] = $code_temp->code;
    }

    $form['table'][$nodes->item_order]['order'] = array(
      '#value' => $order,
    );
    $form['table'][$nodes->item_order]['iid'] = array(
      '#value' => $nodes->item_nid,
    );
    $form['table'][$nodes->item_order]['itid'] = array(
      '#type' => 'value',
      '#value' => $nodes->item_nid,
    );
    $form['table'][$nodes->item_order]['name'] = array(
      '#value' => $nodes->title,
    );
    $form['table'][$nodes->item_order]['link'] = array(
      '#value' => l(t('edit'), $current_path . '/' .
      $nodes->item_nid),
    );
    $form['table'][$nodes->item_order]['display order radio'] = array(
      '#type' => 'select',
      '#default_value' => $nodes->display_order,
      '#options' => array(
        DEEPSURVEY_QUESTION_NORM => t('Normal'),
        DEEPSURVEY_QUESTION_RAND => t('Randomised'),
        DEEPSURVEY_QUESTION_ROT => t('Rotated'),
      ),
    );
    $form['table'][$nodes->item_order]['unique radio'] = array(
      '#type' => 'select',
      '#default_value' => $nodes->unique_question,
      '#options' => array(
        DEEPSURVEY_QUESTION_UNDIFF => t('Undifferentiated'),
        DEEPSURVEY_QUESTION_UNIQ => t('Unique'),
      ),
    );
    $form['table'][$nodes->item_order]['select axis'] = array(
      '#type' => 'select',
      '#default_value' => $nodes->axis,
      '#options' => array(
        DEEPSURVEY_QUESTION_X => t('X-axis'),
        DEEPSURVEY_QUESTION_Y => t('Y-axis'),
        DEEPSURVEY_QUESTION_DROP => t('Drop-down'),
      ),
    );
    $form['table'][$nodes->item_order]['select type'] = array(
      '#type' => 'select',
      '#default_value' => $nodes->response_type,
      '#options' => array(
        DEEPSURVEY_QUESTION_SINGLE => t('Single'),
        DEEPSURVEY_QUESTION_MULTI => t('Multi'),
        DEEPSURVEY_QUESTION_TXT => t('Text'),
        DEEPSURVEY_QUESTION_NUM => t('Numeric'),
      ),
    );
    $form['table'][$nodes->item_order]['reorder box'] = array(
      '#type' => 'textfield',
      '#size' => 4,
      '#maxlength' => 4,
    );
    $form['table'][$nodes->item_order]['remove box'] = array(
      '#type' => 'checkbox',
    );
    $form['table'][$nodes->item_order]['add new'] = array(
      '#value' => l(t('add a new answer list here'),
      $current_path . '/new/' . $nodes->item_order),
    );
    $form['table'][$nodes->item_order]['add existing'] = array(
      '#value' => l(t('add existing answer lists here'),
      $current_path . '/existing/' . $nodes->item_order),
    );
  };

  // Check for duplicate codes.
  foreach ($codes as $code_axis) {
    while (count($code_axis) > 0) {
      $poped_code = array_pop($code_axis);
      if (array_search($poped_code, $code_axis)) {
        switch (key($codes)) {
          case 1:
            drupal_set_message(t('Duplicate codes detected in X axis: This can result in unpredicable survey behaviour'), 'warning');
            break;

          case 2:
            drupal_set_message(t('Duplicate codes detected in Y axis: This can result in unpredicable survey behaviour'), 'warning');
            break;

          case 3:
            drupal_set_message(t('Duplicate codes detected in drop down: This can result in unpredicable survey behaviour'), 'warning');
        }
        $code_axis = array();
      }
    }
  }

  $form['last page'] = array(
    '#type' => 'value',
    '#value' => $last_page,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Done'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
  );

  return $form;
}

/**
 * Submit function for survey question edit form.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_question_deepsurvey_edit_form_submit($form, &$form_state) {
  deepsurvey_edit_process_question($form, $form_state,
    'deepsurvey_question_node_properties');
}

/**
 * Calls the question edit form validation function.
 *
 * Validate function for survey question edit form - I'm using the standard
 * function for this one - not sure if i need to do anything for the drop down
 * boxes.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_question_deepsurvey_edit_form_validate($form, &$form_state) {
  deepsurvey_edit_validate_standard($form, $form_state);
}

/**
 * Processes the edit question form.
 *
 * This is the main function where the processing of the edit question form
 * takes place, it has been disentangled from the node specific function so
 * other deep survey modules can use it.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 *
 * @param array $node_table_properties
 *   String containing the name of the particular node properties table for
 *   this type.
 */
function deepsurvey_edit_process_question($form, &$form_state, $node_table_properties) {

  if ($form_state['clicked_button']['#value'] == t('Done')) {
    // Flag any reorder boxes being set.
    $local_flag = FALSE;
    $reset_flag = FALSE;
    $local_order = 1;
    $local_prev = 0;
    $local_offset = 0;
    $new_id = 0;
    // Item id of the reordered items.
    $new_order = array();
    // Relationship id of the items not being reordered.
    $old_order = array();
    reset($form_state['values']['table']);

    while (list($key, $form_order) = each($form_state['values']['table'])) {
      if ($form_state['values']['table'][$key]['remove box'] == TRUE) {
        db_query('
          DELETE FROM {deepsurvey_question_node_relationship}
          WHERE item_order = %d
          AND deepsurvey_nid = %d', $key, $form_state['values']['nid']);
      }
      elseif ($form_state['values']['table'][$key]['reorder box'] != '') {
        // Remove item from current order.
        db_query('
          DELETE FROM {deepsurvey_question_node_relationship}
          WHERE item_order = %d
          AND deepsurvey_nid = %d', $key, $form_state['values']['nid']);
        $new_order[$form_state['values']['table'][$key]['reorder box']] = array(
          $form_state['values']['table'][$key]['itid'],
          $form_state['values']['table'][$key]['select axis'],
          $form_state['values']['table'][$key]['display order radio'],
          $form_state['values']['table'][$key]['unique radio'],
          $form_state['values']['table'][$key]['select type'],
        );
        $local_flag = TRUE;
      }
      else {
        $old_order[$local_order] = $key;
        $local_order++;
        // Just update everything for now, optimise later if need be.
        db_query('
          UPDATE {deepsurvey_question_node_relationship}
          SET axis = "%s", display_order = %d,
          unique_question = %d, response_type = "%s"
          WHERE item_order = %d AND deepsurvey_nid=%d',
          $form_state['values']['table'][$key]['select axis'],
          $form_state['values']['table'][$key]['display order radio'],
          $form_state['values']['table'][$key]['unique radio'],
          $form_state['values']['table'][$key]['select type'], $key,
          $form_state['values']['nid']);
      }
    }

    // We now have an array of the survey item order, one with the order of the
    // items whose order was not changed and one with the changed order items.
    // Now compare the two and make changes to the database where necessary.
    $local_order = 1;
    if ($local_flag) {
      while ($old_order[$local_order - $local_offset] != FALSE || $new_order[$local_order] != FALSE) {
        if ($old_order[$local_order - $local_offset] != FALSE && $new_order[$local_order] != FALSE) {
          if ($local_prev >= $old_order[$local_order - $local_offset] + 1) {
            $loop_prev = $local_prev;
            $looper = 0;
            while ($loop_prev >= $old_order[$local_order - $local_offset + $looper] + 1) {
              db_query('
                UPDATE {deepsurvey_question_node_relationship}
                SET item_order = %d
                WHERE item_order = %d AND deepsurvey_nid=%d',
                $old_order[$local_order - $local_offset + $looper],
                $old_order[$local_order - $local_offset + $looper] + 1,
                $form_state['values']['nid']);
              $old_order[$local_order - $local_offset + $looper]++;
              $loop_prev = $old_order[$local_order - $local_offset + $looper];
              $looper++;
              $reset_flag = TRUE;
            }
          }
          $new_id = floor($local_prev +
            (($old_order[$local_order - $local_offset] - $local_prev) / 2));
          db_query('
            INSERT INTO {deepsurvey_question_node_relationship}
            (deepsurvey_nid, item_nid, item_order, axis,
            display_order, unique_question, response_type)
            VALUES (%d, %d, %d, "%s", %d, %d, "%s")',
            $form_state['values']['nid'],
            $new_order[$local_order][0],
            $new_id, $new_order[$local_order][1],
            $new_order[$local_order][2],
            $new_order[$local_order][3],
            $new_order[$local_order][4]);
          $local_offset++;
          $local_prev = $new_id;
        }
        elseif ($old_order[$local_order - $local_offset] == FALSE && $new_order[$local_order] != FALSE) {
          if ($local_prev < DEEPSURVEY_MAX_ITEMS) {
            $new_id = floor($local_prev + ((DEEPSURVEY_MAX_ITEMS -
              $local_prev) / 2));
            db_query('
              INSERT INTO {deepsurvey_question_node_relationship}
              (deepsurvey_nid, item_nid, item_order, axis,
              display_order, unique_question, response_type)
              VALUES (%d, %d, %d, "%s", %d, %d, "%s")',
              $form_state['values']['nid'],
              $new_order[$local_order][0], $new_id,
              $new_order[$local_order][1],
              $new_order[$local_order][2],
              $new_order[$local_order][3],
              $new_order[$local_order][4]);
            $local_prev = $new_id;
          }
          else {
            $reset_flag = TRUE;
          }
        }
        else {
          $local_prev = $old_order[$local_order - $local_offset];
        }
        $local_order++;
      }

      if ($reset_flag) {
        _deepsurvey_reset_order('deepsurvey_question_node_relationship',
          $form_state['values']['nid']);
      }

    }
    menu_rebuild();
    drupal_set_message(t('Survey Question Updated'));
  }
  else {
    $form_state['redirect'] = $form_state['values']['last page'];
  }
}


/**
 * Build the form that allows the user to add existing nodes to a survey.
 *
 * @param array $form_state
 *   The state of the form.
 *
 * @param int $deepsurvey_item_nid
 *   If $order is 0 then this is the node id of the survey object the node(s) is
 *   being added to, otherwise it is the item/survey relationship that the nodes
 *   are to be inserted after.
 *
 * @param int $order
 *   The order of the node before the new node(s) or the value of the next
 *   relationship id if order isn't 0.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form array.
 */
function deepsurvey_question_add_existing_form($form_state, $deepsurvey_item_nid, $order, $current_path, $last_page) {

  $deepsurvey_title = db_fetch_object(db_query('
    SELECT title FROM {node}
    WHERE nid = %d', $deepsurvey_item_nid));

  drupal_set_title(t("Add existing answer lists to @name",
    array('@name' => check_plain($deepsurvey_title->title))));

  // Store parameters we are going to need when processing the form.
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $deepsurvey_item_nid,
  );
  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );

  $form['table'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#theme' => 'deepsurvey_add_existing_questions_table',
  );

  $result = db_query('
          SELECT n.nid, n.title, u.name, n.created
          FROM {node} n
          INNER JOIN {users} u
          ON u.uid = n.uid
          WHERE n.type = "deepsurvey_answer_list"');
  while ($nodes = db_fetch_object($result)) {
    $form['table'][$nodes->nid]['name'] = array(
      '#value' => t($nodes->title),
    );
    $form['table'][$nodes->nid]['created'] = array(
      '#value' => t($nodes->created),
    );
    $form['table'][$nodes->nid]['select box'] = array(
      '#type' => 'checkbox',
      // '#title' => 'select',
    );
  };

  $form['last page'] = array(
    '#type' => 'value',
    '#value' => $last_page . '/' . $deepsurvey_item_nid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Done'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Deepsurvey_question_add_existing submit function.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_question_add_existing_form_submit($form, &$form_state) {

  if ($form_state['clicked_button']['#value'] == t('Done')) {
    $local_last = $form_state['values']['order'];
    $next_item_in_order = db_fetch_object(db_query('
      SELECT item_order FROM {deepsurvey_question_node_relationship}
      WHERE deepsurvey_nid = %d AND item_order>%d
      ORDER BY item_order',
      $form_state['values']['rid'],
      $form_state['values']['order']));
    $local_next = $next_item_in_order->item_order;
    $reset_flag = FALSE;

    // In case survey has no items.
    if (!$local_next) {
      $local_next = DEEPSURVEY_MAX_ITEMS;
    }

    foreach (element_children($form_state['values']['table']) as $nid) {
      if ($form_state['values']['table'][$nid]['select box'] == TRUE) {
        if ($local_last >= $local_next + 1) {
          $loop_prev = $local_last;
          $loop_next = $local_next;
          $local_next++;
          while ($loop_prev >= $loop_next + 1) {
            db_query('
              UPDATE {deepsurvey_question_node_relationship}
              SET item_order = %d
              WHERE item_order = %d', $loop_next, $loop_next + 1);
            $loop_prev = $loop_next + 1;
            $loop_next = db_fetch_object(db_query('
              SELECT item_order
              FROM {deepsurvey_question_node_relationship}
              WHERE deepsurvey_nid = %d AND item_order>%d
              ORDER BY item_order',
              $form_state['values']['rid'], $loop_prev));
            $reset_flag = TRUE;
          }
        }
        $new_id = floor($local_last + (($local_next - $local_last) / 2));
        db_query('
          INSERT INTO {deepsurvey_question_node_relationship}
          (deepsurvey_nid, item_nid, item_order, axis, display_order,
          unique_question, response_type)
          VALUES (%d, %d, %d, "%s", %d, %d, "%s")',
          $form_state['values']['rid'],
          $nid, $new_id, DEEPSURVEY_QUESTION_X, DEEPSURVEY_QUESTION_NORM,
          DEEPSURVEY_QUESTION_UNDIFF, DEEPSURVEY_QUESTION_SINGLE);
        $local_last = $new_id;
      }
    }

    if ($reset_flag) {
      _deepsurvey_reset_order('deepsurvey_question_node_relationship',
        $form_state['values']['nid']);
    }

    drupal_set_message(t('New items have been added to the survey question'));
    menu_rebuild();
  }
  $form_state['redirect'] = $form_state['values']['last page'];
}

/**
 * Builds the form for choosing the new node type to create.
 *
 * Build the form that allows the user to choose which type of node to create
 * and add to a survey.
 *
 * @param array $form_state
 *   The state of the form.
 *
 * @param int $deepsurvey_item_nid
 *   If $order is 0 then this is the node id of the survey object the node is
 *   being added to, otherwise is it the it is the item/survey relationship that
 *   the node is to be inserted after.
 *
 * @param int $order
 *   The order of the node before the new node(s) or the value of the next
 *   relationship id if order isn't 0.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form array.
 */
function deepsurvey_question_add_new_form($form_state, $deepsurvey_item_nid, $order, $current_path, $last_page) {

  $deepsurvey_title = db_fetch_object(db_query('
    SELECT title FROM {node}
    WHERE nid = %d', $deepsurvey_item_nid));

  drupal_set_title(t("Add a new answer list to @name",
    array('@name' => check_plain($deepsurvey_title->title))));

  // Store parameters we are going to need when processing the form.
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $deepsurvey_item_nid,
  );
  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );

  $form['confirm'] = array(
    '#value' => t('Are you sure you want to create a new answer list?'),
    '#prefix' => '<p>',
    '#suffix' => '</p>',
  );

  $form['last page'] = array(
    '#type' => 'value',
    '#value' => $last_page . '/' . $deepsurvey_item_nid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Yes'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('No'),
  );

  return $form;
}

/**
 * Deepsurvey_add_new submit function.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_question_add_new_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Yes')) {
    $_SESSION['deepsurvey_question_new_rid'] = $form_state['values']['rid'];
    $_SESSION['deepsurvey_question_new_order'] = $form_state['values']['order'];
    $_SESSION['deepsurvey_question_new_table']
      = 'deepsurvey_question_node_relationship';
    $_SESSION['deepsurvey_last_page'] = $form_state['values']['last page'];
    $form_state['redirect'] = 'node/add/deepsurvey-answer-list';
  }
  else {
    $form_state['redirect'] = $form_state['values']['last page'];
  }

}

/**
 * Removes deleted deepsurvey nodes.
 *
 * Remove deleted deepsurvey content type node from tables in which they are
 * referenced. Called from each node types' hook_delete.
 *
 * @param int $deleted_node
 *   The node to be removed from tables.
 *
 * @return bool
 *   return false if any problems were encountered otherwise true
 */
function deepsurvey_question_delete_content_type_node($deleted_node) {
  $rc = db_query('
    DELETE FROM {deepsurvey_question_node_relationship}
    WHERE item_nid = %d', $deleted_node);

  if ($rc) {
    return TRUE;
  }
  else {
    return FALSE;
  }

}
