<?php

/**
 * @file
 * User interface for Deep Survey Question module.
 *
 * Functions purely to display questions in the deepsurvey module.
 */

// DEEP SURVEY QUESTION USER SPECIFIC FUNCTIONS.
// THEME FUNCTIONS.
// Remember to updated deepsurvey_theme() in survey.module.
/**
 * Theme the question which has a single row of answers.
 */
function theme_deepsurvey_question_display_table_singlerow($form) {

  $headers = array();

  $rows = array();

  if (!empty($form['code'])) {

    foreach (element_children($form['headings']) as $heading) {
      $headers[] = drupal_render($form['headings'][$heading]);
    }
    $rows[0] = array(
      'data' => array(),
    );
    foreach (element_children($form['code']) as $code) {
      if ($form['type'][$code][0]['col']['#value'] == DEEPSURVEY_QUESTION_SINGLE && $form['code'][$code][0]['#type'] != 'select') {
        foreach (element_children($form['code'][$code][0]) as $each_radio_button) {
          $rows[0]['data'][]
            = drupal_render($form['code'][$code][0][$each_radio_button]);
        }
      }
      else {
        $rows[0]['data'][] = drupal_render($form['code'][$code][0]);
      }
    }

  }
  $table = theme('table', $headers, $rows,
    array('style' => 'table-layout:fixed;overflow:hidden;'));
  return $table . drupal_render($form);
}

/**
 * Theme the question which has a single column of answers.
 */
function theme_deepsurvey_question_display_table_singlecol($form) {

  $headers = array(
    array('data' => '', 'style' => 'background-color:transparent;border-style:none;'),
    array('data' => '', 'style' => 'background-color:transparent;border-style:none;'));

  $rows = array();

  $radio_key = 0;

  if (!empty($form['code'])) {

    foreach (element_children($form['headings']) as $key) {
      if (($form['type'][0][$key]['row']['#value'] == DEEPSURVEY_QUESTION_SINGLE || $radio_key != 0) && $form['code'][0][$key]['#type'] != 'select') {
        if ($form['type'][0][$key]['row']['#value'] == DEEPSURVEY_QUESTION_SINGLE) {
          $radio_key = $key;
        }
        $rows[] = array(
          'data' => array(
            drupal_render($form['code'][0][$radio_key]['0-' . $key]),
            drupal_render($form['headings'][$key], 2, FALSE),
          ),
          'style' => 'background-color:transparent;',
        );
      }
      else {
        if ($form['type'][0][$key]['row']['#value'] == DEEPSURVEY_QUESTION_TXT) {
          $rows[] = array(
            'data' => array(
              drupal_render($form['headings'][$key]),
              drupal_render($form['code'][0][$key]),
            ),
            'style' => 'background-color:transparent;border-style:none;',
          );
        }
        else {
          $rows[] = array(
            'data' => array(
              drupal_render($form['code'][0][$key]),
              drupal_render($form['headings'][$key]),
            ),
            'style' => 'background-color:transparent;border-style:none;',
          );
        }
        $radio_key = 0;
      }
    }
  }
  $table = theme('table', $headers, $rows, array(
    'style' =>
      'table-layout:fixed;overflow:hidden;width:auto;border-collapse:separate;border-style:none;'));
  return $table . drupal_render($form);
}

/**
 * Theme the question which has both columns and rows.
 *
 * Use #size value to limit the length of the textbox (in form definition).
 */
function theme_deepsurvey_question_display_table_grid($form) {

  $headers = array('');

  $rows = array();

  $radio_key_col = reset(element_children($form['headings']['col']));
  $radio_key_row = reset(element_children($form['headings']['row']));

  if (!empty($form['code'])) {

    foreach (element_children($form['headings']['col']) as $heading) {
      $headers[] = drupal_render($form['headings']['col'][$heading]);
    }

    foreach (element_children($form['headings']['row']) as $row) {
      $current_row = array(drupal_render($form['headings']['row'][$row]));
      foreach (element_children($form['headings']['col']) as $col) {
        if ((!$form['code'][$col][$row]
             || ($form['type'][$col][$row]['row']['#value'] == DEEPSURVEY_QUESTION_SINGLE
                && $form['type'][$col][$row]['col']['#value'] == DEEPSURVEY_QUESTION_SINGLE)
             || ($form['type'][$col][$row]['row']['#value'] == DEEPSURVEY_QUESTION_SINGLE
                && $form['type'][$col][$row]['col']['#value'] == DEEPSURVEY_QUESTION_MULTI)
             || ($form['type'][$col][$row]['row']['#value'] == DEEPSURVEY_QUESTION_MULTI
                 && $form['type'][$col][$row]['col']['#value'] == DEEPSURVEY_QUESTION_SINGLE))
            && $form['code'][$col][$row]['#type'] != 'select') {
          if ($form['type'][$col][$row]['row']['#value'] == DEEPSURVEY_QUESTION_SINGLE) {
            $radio_key_row = $row;
          }

          if ($form['type'][$col][$row]['col']['#value'] == DEEPSURVEY_QUESTION_SINGLE) {
            $radio_key_col = $col;
          }
          elseif ($form['type'][$col][$radio_key_row]['col']['#value'] == DEEPSURVEY_QUESTION_SINGLE) {
            $radio_key_col = $col;
          }

          $current_row[] = drupal_render(
            $form['code'][$radio_key_col][$radio_key_row][$col . '-' . $row]);
        }
        else {
          $current_row[] = drupal_render($form['code'][$col][$row]);
        }
      }
      $rows[] = array(
        'data' => $current_row,
      );
    }

  }
  $table = theme('table', $headers, $rows,
    array('style' => 'table-layout:fixed;overflow:hidden;'));
  return $table . drupal_render($form);
}
