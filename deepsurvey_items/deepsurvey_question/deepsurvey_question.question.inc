<?php

/**
 * @file
 * Question display and processing forms.
 *
 * Question form functions.
 *
 * Contains all the functions to display the questions as forms and process
 * those forms.
 */

/**
 * Implementation of functions for displaying the question in the survey.
 *
 * Called from deepsurvey_view (or other container module).
 *
 * @param int $instance
 *   Question instance.
 *
 * @param int $survey
 *   The node id of the survey.
 *
 * @param int $usr
 *   The user id of the user viewing the survey.
 *
 * @param int $parent_item
 *   The node id of the container type which the question is in (this will not
 *   always be the survey - could be a loop or section).
 *
 * @param string $parent_type
 *   The type of container the question is in (this will not always be the
 *   survey - could be a loop or section).
 *
 * @param string $loops
 *   The current loop value - this will be build the loop type and inserted
 *   into the data table as is.
 *
 * @param array $state_values
 *   The current state values - these are the state valiables of the current
 *   container.
 *
 * @param array $state_values_global
 *   The current global state values - these are the state variables of the
 *   survey.
 *
 * @return sting
 *   Returns the question form.
 */
function deepsurvey_question_deepsurvey_item_display($instance, $survey, $usr, $parent_item, $parent_type, $loops, &$state_values, &$state_values_global) {
  $rc = '';
  $counter = 1;

  // Check for filtering.
  $question_filter = db_query('
    SELECT * FROM {deepsurvey_node_questionmask}
    WHERE userid=%d
    AND deepsurvey_nid=%d
    AND instance = %d
    AND masked_question = %d',
    $usr, $survey, $state_values_global->current_questionmask_instance,
    $state_values->current_question);

  if (!$question_filter->deepsurvey_nid) {
    $rc = drupal_get_form('deepsurvey_question_deepsurvey_item_form',
      $instance, $survey, $usr, $parent_item, $parent_type, $loops,
      $state_values, $state_values_global);
  }
  else {
    if ($state_values_global->current_direction) {
      if (!_deepsurvey_next_item($parent_item, $parent_type, $state_values->current_question, $state_values->current_item_instance, $usr)) {
        $rc = ':#end';
      }
    }
    else {
      if (!_deepsurvey_previous_item($survey, $parent_item, $parent_type, $current_question, $state_values->current_item_instance, $usr, $state_values_global->current_mask_instance, $state_values_global->current_scalemask_instance, $state_values_global->current_questionmask_instance, $state_values_global->current_substitution_instance)) {
        $rc = ':#begin';
      }
    }
  }

  return $rc;
}

/**
 * Builds the question form for the previous function.
 *
 * @param array $form_state
 *   The form state array.
 *
 * @param int $instance
 *   Question instance.
 *
 * @param int $survey
 *   The node id of the survey.
 *
 * @param int $usr
 *   The user id of the user viewing the survey.
 *
 * @param int $parent_item
 *   The node id of the container type which the question is in (this will not
 *   always be the survey - could be a loop or section).
 *
 * @param string $parent_type
 *   The type of container the question is in (this will not alwasy be the
 *   survey - could be a loop or section).
 *
 * @param string $loops
 *   The current loop value - this will be build be the loop type and inserted
 *   into the data table as is.
 *
 * @param array $state_values
 *   The current state values - these are the state valiables of the current
 *   container.
 *
 * @param array $state_values_global
 *   The current global state values - these are the state variables of the
 *   survey.
 *
 * @return string
 *   Returns the question form.
 */
function deepsurvey_question_deepsurvey_item_form($form_state, $instance, $survey, $usr, $parent_item, $parent_type, $loops, $state_values, $state_values_global) {
  $form = deepsurvey_question_deepsurvey_item_form_build_question($instance,
    $survey, $usr, $parent_item, $parent_type, $loops, $state_values,
    $state_values_global);

  $form['user'] = array(
    '#type' => 'value',
    '#value' => $usr,
  );

  $form['survey'] = array(
    '#type' => 'value',
    '#value' => $survey,
  );

  $form['item'] = array(
    '#type' => 'value',
    '#value' => $state_values->current_question,
  );

  $form['item_instance'] = array(
    '#type' => 'value',
    '#value' => $state_values->current_item_instance,
  );

  $form['parent'] = array(
    '#type' => 'value',
    '#value' => $parent_item,
  );

  $form['parent_type'] = array(
    '#type' => 'value',
    '#value' => $parent_type,
  );

  $form['mask_instance'] = array(
    '#type' => 'value',
    '#value' => $state_values_global->current_mask_instance,
  );

  $form['scalemask_instance'] = array(
    '#type' => 'value',
    '#value' => $state_values_global->current_scalemask_instance,
  );

  $form['questionmask_instance'] = array(
    '#type' => 'value',
    '#value' => $state_values_global->current_questionmask_instance,
  );

  $form['substitution_instance'] = array(
    '#type' => 'value',
    '#value' => $state_values_global->current_substitution_instance,
  );

  $back_valid = db_fetch_object(db_query('
    SELECT backwards_navigation FROM {deepsurvey_node_properties}
    WHERE nid = %d', $survey));
  if ($back_valid->backwards_navigation) {
    $form['Previous'] = array(
      '#type' => 'submit',
      '#value' => t('back'),
    );
  }

  $form['Next'] = array(
    '#type' => 'submit',
    '#value' => t('forward'),
  );

  return $form;

}

/**
 * More form building.
 *
 * This has been split from the above function so that other items can display
 * multiple questions.
 *
 * @param int $instance
 *   Question instance.
 *
 * @param int $survey
 *   The node id of the survey.
 *
 * @param int $usr
 *   The user id of the user viewing the survey.
 *
 * @param int $parent_item
 *   The node id of the container type which the question is in (this will not
 *   always be the survey - could be a loop or section).
 *
 * @param string $parent_type
 *   The type of container the question is in (this will not alwasy be the
 *   survey - could be a loop or section).
 *
 * @param string $loops
 *   The current loop value - this will be build be the loop type and inserted
 *   into the data table as is.
 *
 * @param array $state_values
 *   The current state values - these are the state valiables of the current
 *   container.
 *
 * @param array $state_values_global
 *   The current global state values - these are the state variables of the
 *   survey.
 *
 * @return array
 *   Returns the question form.
 */
function deepsurvey_question_deepsurvey_item_form_build_question($instance, $survey, $usr, $parent_item, $parent_type, $loops, &$state_values, &$state_values_global) {
  $current_data = array();
  $seed_value = array();
  $prev_data_exists = FALSE;
  $question_attributes = db_fetch_object(db_query('
    SELECT * FROM {deepsurvey_question_node_properties}
    WHERE nid = %d', $state_values->current_question));

  $form['question'] = array(
    '#value' => t(check_markup($question_attributes->question_text, 2, FALSE)),
    '#theme' => 'item',
  );
  $form['instruction'] = array(
    '#prefix' => '<span style="font-style:italic;">',
    '#suffix' => '</span>',
    '#value' => t(check_plain($question_attributes->instruction_text)),
    '#theme' => 'item',
  );

  // Check for previous data.
  $prev_data_text = db_query('
    SELECT * FROM {deepsurvey_node_data_text}
    WHERE userid = %d
    AND deepsurvey_nid = %d
    AND instance = %d
    AND loops = "%s"
    AND question_nid = %d',
    $usr, $survey, $instance, $loops, $state_values->current_question);
  while ($temp = db_fetch_object($prev_data_text)) {
    $current_data[$temp->row_code][$temp->column_code] = $temp->response;
    $prev_data_exists = TRUE;
  }

  $prev_data_numeric = db_query('
    SELECT * FROM {deepsurvey_node_data_numeric}
    WHERE userid = %d
    AND deepsurvey_nid = %d
    AND instance = %d
    AND loops = "%s"
    AND question_nid = %d',
    $usr, $survey, $instance, $loops, $state_values->current_question);
  while ($temp = db_fetch_object($prev_data_numeric)) {
    $current_data[$temp->row_code][$temp->column_code] = $temp->response;
    $prev_data_exists = TRUE;
  }

  $prev_data_standard = db_query('
    SELECT * FROM {deepsurvey_node_data_standard}
    WHERE userid = %d
    AND deepsurvey_nid = %d
    AND instance = %d
    AND loops = "%s"
    AND question_nid = %d',
    $usr, $survey, $instance, $loops, $state_values->current_question);
  while ($temp = db_fetch_object($prev_data_standard)) {
    $current_data[$temp->row_code][$temp->column_code] = $temp->response;
    $prev_data_exists = TRUE;
  }

  // Create a list of rows.
  $row_heading_boundaries = array();
  $row_items
    = _deepsurvey_question_get_axis_array($state_values->current_question,
    DEEPSURVEY_QUESTION_X, $state_values_global->random_seed,
    $state_values_global->rotation_value, $row_heading_boundaries);

  // Create a list of columns.
  $column_heading_boundaries = array();
  $column_items
    = _deepsurvey_question_get_axis_array($state_values->current_question,
    DEEPSURVEY_QUESTION_Y, $state_values_global->random_seed,
    $state_values_global->rotation_value, $column_heading_boundaries);

  // Create a list of drop-down responses.
  $drop_heading_boundaries = array();
  $drop_items
    = _deepsurvey_question_get_axis_array($state_values->current_question,
    DEEPSURVEY_QUESTION_DROP, $state_values_global->random_seed,
    $state_values_global->rotation_value, $drop_heading_boundaries);
  // Really only need an array list.
  $drop_down = array();
  foreach ($drop_items as $drop) {
    $drop_down[$drop['code']] = $drop['text'];
  }

  $form['table'] = array(
    '#tree' => TRUE,
  );

  // Build the response lists - cases for no x or y axis included.
  //
  // Lets us know if the foreach is currently processing a single response list
  // - which is handled differently to the other types.
  $single_response_flag_row = FALSE;
  $single_response_code_row = 0;
  $single_response_flag_col = FALSE;
  $single_response_code_col = 0;

  // Create answers in a single row.
  if (!$row_items) {
    $form['table'][$state_values->current_question] = array(
      '#tree' => TRUE,
      '#theme' => 'deepsurvey_question_display_table_singlerow',
    );
    foreach ($column_items as $col) {
      $form['table'][$state_values->current_question]['headings'][$col['code']] = array(
        '#value' => $col['text'],
      );
    }
    foreach ($column_items as $col) {
      if ($col['type'] == DEEPSURVEY_QUESTION_SINGLE  && empty($drop_down)) {
        if (!$single_response_flag_col || $column_heading_boundaries[$col['code']]) {
          $form['table'][$state_values->current_question]['code'][$col['code']][0] = _deepsurvey_question_get_form_item($drop_down, $current_data[0][$col['code']], $col);
          $form['table'][$state_values->current_question]['type'][$col['code']][0]['col'] = array(
            '#type' => 'value',
            '#value' => $col['type'],
          );
          $single_response_code_col = $col['code'];
          $single_response_flag_col = TRUE;
        }
        $form['table'][$state_values->current_question]['code'][$single_response_code_col][0]['#options'][$col['code'] . '-0'] = '';
        if ($current_data[0][$col['code']]) {
          $form['table'][$state_values->current_question]['code'][$single_response_code_col][0]['#default_value'] = $col['code'] . '-0';
        }
      }
      else {
        if ($single_response_flag_col) {
          $single_response_flag_col = FALSE;
        }
        $form['table'][$state_values->current_question]['code'][$col['code']][0] = _deepsurvey_question_get_form_item($drop_down, $current_data[0][$col['code']], $col);
        $form['table'][$state_values->current_question]['type'][$col['code']][0]['col'] = array(
          '#type' => 'value',
          '#value' => $col['type'],
        );
      }
    }
  }
  // Create answers in a single column.
  elseif (!$column_items) {
    $form['table'][$state_values->current_question] = array(
      '#tree' => TRUE,
      '#theme' => 'deepsurvey_question_display_table_singlecol',
    );
    foreach ($row_items as $row) {
      $form['table'][$state_values->current_question]['headings'][$row['code']] = array(
        '#value' => $row['text'],
      );
      if ($row['type'] == DEEPSURVEY_QUESTION_SINGLE && empty($drop_down)) {
        if (!$single_response_flag_row || $row_heading_boundaries[$row['code']]) {
          $form['table'][$state_values->current_question]['code'][0][$row['code']] = _deepsurvey_question_get_form_item($drop_down, $current_data[$row['code']][0], $row);
          $form['table'][$state_values->current_question]['type'][0][$row['code']]['row'] = array(
            '#type' => 'value',
            '#value' => $row['type'],
          );
          $single_response_code_row = $row['code'];
          $single_response_flag_row = TRUE;
        }
        $form['table'][$state_values->current_question]['code'][0][$single_response_code_row]['#options']['0-' . $row['code']] = '';
        if ($current_data[$row['code']][0]) {
          $form['table'][$state_values->current_question]['code'][0][$single_response_code_row]['#default_value'] = '0-' . $row['code'];
        }
      }
      else {
        if ($single_response_flag_row) {
          $single_response_flag_row = FALSE;
        }
        $form['table'][$state_values->current_question]['code'][0][$row['code']] = _deepsurvey_question_get_form_item($drop_down, $current_data[$row['code']][0], $row);
        $form['table'][$state_values->current_question]['type'][0][$row['code']]['row'] = array(
          '#type' => 'value',
          '#value' => $row['type'],
        );
      }
    }
  }
  // Create answers in rows and columns.
  else {
    $form['table'][$state_values->current_question] = array(
      '#tree' => TRUE,
      '#theme' => 'deepsurvey_question_display_table_grid',
    );
    foreach ($column_items as $col) {
      $form['table'][$state_values->current_question]['headings']['col'][$col['code']] = array(
        '#value' => $col['text'],
      );
    }
    foreach ($row_items as $row) {
      $form['table'][$state_values->current_question]['headings']['row'][$row['code']] = array(
        '#value' => $row['text'],
      );

      $single_response_flag_row = FALSE;
      if ($col['type'] != DEEPSURVEY_QUESTION_SINGLE && empty($drop_down)) {
        $single_response_flag_col = FALSE;
      }

      foreach ($column_items as $col) {

        if ($row['type'] == DEEPSURVEY_QUESTION_SINGLE && $col['type'] == DEEPSURVEY_QUESTION_SINGLE && empty($drop_down)) {
          if ((!$single_response_flag_row || $row_heading_boundaries[$row['code']]) && (!$single_response_flag_col || $column_heading_boundaries[$col['code']])) {
            $form['table'][$state_values->current_question]['code'][$col['code']][$row['code']] = _deepsurvey_question_get_form_item($drop_down, $current_data[$row['code']][$col['code']], $row, $col);
            $form['table'][$state_values->current_question]['type'][$col['code']][$row['code']]['row'] = array(
              '#type' => 'value',
              '#value' => $row['type'],
            );
            $form['table'][$state_values->current_question]['type'][$col['code']][$row['code']]['col'] = array(
              '#type' => 'value',
              '#value' => $col['type'],
            );
            $single_response_code_row = $row['code'];
            $single_response_flag_row = TRUE;
            $single_response_code_col = $col['code'];
            $single_response_flag_col = TRUE;
          }
          elseif (!$single_response_flag_row || $row_heading_boundaries[$row['code']]) {
            $single_response_code_col = $col['code'];
            $single_response_flag_row = TRUE;
          }
          $form['table'][$state_values->current_question]['code'][$single_response_code_col][$single_response_code_row]['#options'][$col['code'] . '-' . $row['code']] = '';
          if ($current_data[$row['code']][$col['code']]) {
            $form['table'][$state_values->current_question]['code'][$single_response_code_col][$single_response_code_row]['#default_value'] = $col['code'] . '-' . $row['code'];
          }
        }
        elseif ($row['type'] == DEEPSURVEY_QUESTION_SINGLE && $col['type'] == DEEPSURVEY_QUESTION_MULTI && empty($drop_down)) {
          if ((!$single_response_flag_row || $row_heading_boundaries[$row['code']] || $single_response_flag_col)) {
            $form['table'][$state_values->current_question]['code'][$col['code']][$row['code']] = _deepsurvey_question_get_form_item($drop_down, $current_data[$row['code']][$col['code']], $row, $col);
            $form['table'][$state_values->current_question]['type'][$col['code']][$row['code']]['row'] = array(
              '#type' => 'value',
              '#value' => $row['type'],
            );
            $form['table'][$state_values->current_question]['type'][$col['code']][$row['code']]['col'] = array(
              '#type' => 'value',
              '#value' => $col['type'],
            );
            $single_response_code_col = $col['code'];
            $single_response_flag_row = TRUE;
            $single_response_flag_col = FALSE;
          }
          $form['table'][$state_values->current_question]['code'][$single_response_code_col][$row['code']]['#options'][$col['code'] . '-' . $row['code']] = '';
          if ($current_data[$row['code']][$col['code']]) {
            $form['table'][$state_values->current_question]['code'][$single_response_code_col][$row['code']]['#default_value'] = $col['code'] . '-' . $row['code'];
          }
        }
        elseif ($col['type'] == DEEPSURVEY_QUESTION_SINGLE && $row['type'] == DEEPSURVEY_QUESTION_MULTI && empty($drop_down)) {
          if ((!$single_response_flag_col || $column_heading_boundaries[$col['code']]) || $single_response_flag_row) {
            $form['table'][$state_values->current_question]['code'][$col['code']][$row['code']] = _deepsurvey_question_get_form_item($drop_down, $current_data[$row['code']][$col['code']], $row, $col);
            $form['table'][$state_values->current_question]['type'][$col['code']][$row['code']]['row'] = array(
              '#type' => 'value',
              '#value' => $row['type'],
            );
            $form['table'][$state_values->current_question]['type'][$col['code']][$row['code']]['col'] = array(
              '#type' => 'value',
              '#value' => $col['type'],
            );
            $single_response_code_row = $row['code'];
            $single_response_flag_row = FALSE;
          }
          $form['table'][$state_values->current_question]['code'][$col['code']][$single_response_code_row]['#options'][$col['code'] . '-' . $row['code']] = '';
          if ($current_data[$row['code']][$col['code']]) {
            $form['table'][$state_values->current_question]['code'][$col['code']][$single_response_code_row]['#default_value'] = $col['code'] . '-' . $row['code'];
          }

        }
        else {

          if ($single_response_flag_row) {
            $single_response_flag_row = FALSE;
          }
          if ($single_response_flag_col) {
            $single_response_flag_col = FALSE;
          }
          $form['table'][$state_values->current_question]['code'][$col['code']][$row['code']] = _deepsurvey_question_get_form_item($drop_down, $current_data[$row['code']][$col['code']], $row, $col);
          $form['table'][$state_values->current_question]['type'][$col['code']][$row['code']]['row'] = array(
            '#type' => 'value',
            '#value' => $row['type'],
          );
          $form['table'][$state_values->current_question]['type'][$col['code']][$row['code']]['col'] = array(
            '#type' => 'value',
            '#value' => $col['type'],
          );

        }

      }

      if ($col['type'] == DEEPSURVEY_QUESTION_SINGLE && !$single_response_flag_col && empty($drop_down)) {
        $single_response_flag_col = TRUE;
      }
    }
  }

  // Answer list headings - used currently for validating single response axes
  // but will form actual headings eventually.
  foreach ($column_heading_boundaries as $heading_boundary_value) {
    $form['table'][$state_values->current_question]['boundary'][$heading_boundary_value]['col'] = array(
      '#type' => 'value',
      '#value' => '*heading goes here*',
    );
  }
  foreach ($row_heading_boundaries as $heading_boundary_value) {
    $form['table'][$state_values->current_question]['boundary'][$heading_boundary_value]['row'] = array(
      '#type' => 'value',
      '#value' => '*heading goes here*',
    );
  }

  $form['table'][$state_values->current_question]['loop'] = array(
    '#type' => 'value',
    '#value' => $loops,
  );

  $form['table'][$state_values->current_question]['item_instance'] = array(
    '#type' => 'value',
    '#value' => $state_values->current_item_instance,
  );

  return $form;
}

/**
 * Function for determining the make up and order of the axes.
 *
 * @param int $current_question
 *   The node id of the current question.
 *
 * @param int $axis
 *   Values for the type of axis - x-axis, y-axis or drop down.
 *
 * @param int $seed_value
 *   Seed value for randomly ordered axes.
 *
 * @param int $rot_value
 *   Value to determine the current start point for rotated axes.
 *
 * @param array $heading_boundaries
 *   Dereferenced variable to hold the values indicating where the high level
 *   headings end.
 *
 * @return array
 *   An array of all the answers for an axis in the order they are to be
 *   displayed.
 */
function _deepsurvey_question_get_axis_array($current_question, $axis, $seed_value, $rot_value, &$heading_boundaries) {
  $temp;
  $temp2;
  $iterator = 0;
  $axis_items = array();
  $axis_items_temp = array();
  // Used to check for duplicate codes.
  $current_code;

  $axis_headings = db_query('
    SELECT * FROM {deepsurvey_question_node_relationship}
    WHERE deepsurvey_nid = %d AND axis = %d
    ORDER BY item_order', $current_question, $axis);
  $current_order = 0;
  $current_unique = 0;
  $current_rotation = NULL;
  $current_heading = '';
  while ($temp = db_fetch_object($axis_headings)) {

    // Randomise.
    if ($current_order == DEEPSURVEY_QUESTION_RAND
          && ($temp->display_order != DEEPSURVEY_QUESTION_RAND || $temp->unique_question != DEEPSURVEY_QUESTION_UDIFF || $current_unique != DEEPSURVEY_QUESTION_UDIFF)) {
      // Seed the random number generator.
      srand($seed_value);

      shuffle($axis_items_temp);
      $axis_items = array_merge($axis_items, $axis_items_temp);
    }

    // Rotate.
    if ($current_order == DEEPSURVEY_QUESTION_ROT
          && ($temp->display_order != DEEPSURVEY_QUESTION_ROT || $temp->unique_question != DEEPSURVEY_QUESTION_UDIFF || $current_unique != DEEPSURVEY_QUESTION_UDIFF)) {
      for ($itr = 0; $itr < $rot_value; $itr++) {
        $axis_items_temp[] = array_shift($axis_items_temp);
      }
      $axis_items = array_merge($axis_items, $axis_items_temp);
    }

    $axis_responses = db_query('
      SELECT * FROM {deepsurvey_answer_list_responses}
      WHERE deepsurvey_nid = %d
      ORDER BY item_order', $temp->item_nid);
    $current_heading = db_fetch_object(db_query('
      SELECT heading FROM {deepsurvey_answer_list_node_properties}
      WHERE nid = %d'), $temp2->item_nid);

    while ($temp2 = db_fetch_object($axis_responses)) {
      // Check for duplicate code.
      $current_code = $temp2->code;
      while (array_search($current_code, $axis_items) || array_search($current_code, $axis_items_temp)) {
        $current_code = $current_code * 10;
      }

      $axis_items_temp[$iterator] = array(
        'text' => $temp2->response,
        'code' => $current_code,
        'type' => $temp->response_type,
        'heading' => $current_heading->heading,
      );
      $iterator++;
    }
    $current_order = $temp->display_order;
    $current_unique = $temp->unique_question;
    $current_rotation = $temp->current_rotation;

  }

  // Deal with final $axis_items_temp.
  // Randomise.
  if ($current_order == DEEPSURVEY_QUESTION_RAND) {
    // Seed the random number generator.
    srand($seed_value);

    shuffle($axis_items_temp);
  }

  // Rotate.
  if ($current_order == DEEPSURVEY_QUESTION_ROT) {
    for ($itr = 0; $itr < $rot_value; $itr++) {
      $axis_items_temp[] = array_shift($axis_items_temp);
    }
  }

  $axis_items = array_merge($axis_items, $axis_items_temp);

  return $axis_items;
}

/**
 * Function returns the form item for a particluar response.
 *
 * @param array $drop
 *   Array of values for a drop down box.
 *
 * @param int $data
 *   Current response to that control, if any.
 *
 * @param array $row
 *   Array to define the row value (although not the row if there is only a
 *   single line of responses).
 *
 * @param array $col
 *   Array to define the column (NULL if the question is not a grid (having
 *   both x and y axes).
 *
 * @return array
 *   Returns the form definition of the control.
 */
function _deepsurvey_question_get_form_item($drop, $data, $row, $col = NULL) {
  $rc = array();
  if ($drop && $row['type'] != DEEPSURVEY_QUESTION_TXT
      && $row['type'] != DEEPSURVEY_QUESTION_NUM
      && $col['type'] != DEEPSURVEY_QUESTION_TXT
      && $col['type'] != DEEPSURVEY_QUESTION_NUM) {
    $rc['#type'] = 'select';
    $rc['#default_value'] = $data;
    $rc['#options'] = array('' => '') + $drop;
  }
  elseif ($row['type'] == DEEPSURVEY_QUESTION_TXT || $col['type'] == DEEPSURVEY_QUESTION_TXT) {
    $rc['#type'] = 'textfield';
    $rc['#default_value'] = $data;
    $rc['#size'] = 35;
  }
  elseif ($row['type'] == DEEPSURVEY_QUESTION_NUM || $col['type'] == DEEPSURVEY_QUESTION_NUM) {
    $rc['#type'] = 'textfield';
    $rc['#default_value'] = $data;
    $rc['#size'] = 7;
  }
  elseif ($row['type'] == DEEPSURVEY_QUESTION_SINGLE || $col['type'] == DEEPSURVEY_QUESTION_SINGLE) {
    $rc['#type'] = 'radios';
    $rc['#options'] = array();
  }
  else {
    $rc['#type'] = 'checkbox';
    if ($data == 1) {
      $rc['#default_value'] = 1;
    }
  }

  return $rc;
}

/**
 * Processes the question reponses.
 *
 * Submission of question response - generic bit which will also be called by
 * the node that displays multiple questions on one page.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_question_deepsurvey_item_form_submit_process($form, &$form_state) {

  // Insert values into appropriate locations.
  foreach (element_children($form_state['values']['table']) as $q) {
    $nid = $form_state['values']['table'][$q];

    // Don't process table if no questions exist.
    if (is_array($nid['code'])) {

      // Delete old values.
      db_query('
        DELETE FROM {deepsurvey_node_data_numeric}
        WHERE userid = %d
        AND deepsurvey_nid = %d
        AND instance = %d
        AND loops = "%s"
        AND question_nid = %d',
        $form_state['values']['user'],
        $form_state['values']['survey'], 1, $nid['loop'], $q);
      db_query('
        DELETE FROM {deepsurvey_node_data_text}
        WHERE userid = %d AND deepsurvey_nid = %d
        AND instance = %d AND loops = "%s"
        AND question_nid = %d',
        $form_state['values']['user'],
        $form_state['values']['survey'], 1, $nid['loop'], $q);
      db_query('
        DELETE FROM {deepsurvey_node_data_standard}
        WHERE userid = %d
        AND deepsurvey_nid = %d
        AND instance = %d
        AND loops = "%s"
        AND question_nid = %d',
        $form_state['values']['user'],
        $form_state['values']['survey'], 1, $nid['loop'], $q);

      foreach ($nid['code'] as $col => $columns) {
        foreach ($columns as $row => $response) {
          if ($response != '') {
            if ($nid['type'][$col][$row]['row']['#value'] == DEEPSURVEY_QUESTION_NUM || $nid['type'][$col][$row]['col']['#value'] == DEEPSURVEY_QUESTION_NUM) {
              db_query('
                INSERT INTO {deepsurvey_node_data_numeric}
                (userid, deepsurvey_nid, instance, loops,
                question_nid, column_code, row_code, response)
                VALUES (%d, %d, %d, "%s", %d, %d, %d, %d)',
                $form_state['values']['user'],
                $form_state['values']['survey'], 1, $nid['loop'], $q, $col,
                $row, $response);
            }
            elseif ($nid['type'][$col][$row]['row']['#value'] == DEEPSURVEY_QUESTION_TXT || $nid['type'][$col][$row]['col']['#value'] == DEEPSURVEY_QUESTION_TXT) {
              db_query('
                INSERT INTO {deepsurvey_node_data_text}
                (userid, deepsurvey_nid, instance, loops,
                question_nid, column_code, row_code, response)
                VALUES (%d, %d, %d, "%s", %d, %d, %d, "%s")',
                $form_state['values']['user'],
                $form_state['values']['survey'], 1, $nid['loop'], $q, $col,
                $row, $response);
            }
            elseif ($nid['type'][$col][$row]['row']['#value'] == DEEPSURVEY_QUESTION_SINGLE || $nid['type'][$col][$row]['col']['#value'] == DEEPSURVEY_QUESTION_SINGLE) {
              // Add code to handle drop down boxes as num.
              if (strpos($response, '-') === FALSE) {
                db_query('
                  INSERT INTO {deepsurvey_node_data_standard}
                  (userid, deepsurvey_nid, instance, loops,
                  question_nid, column_code, row_code, response)
                  VALUES (%d, %d, %d, "%s", %d, %d, %d, %d)',
                  $form_state['values']['user'],
                  $form_state['values']['survey'], 1, $nid['loop'], $q, $col,
                  $row, $response);
              }
              else {
                db_query('
                  INSERT INTO {deepsurvey_node_data_standard}
                  (userid, deepsurvey_nid, instance, loops,
                  question_nid, column_code, row_code, response)
                  VALUES (%d, %d, %d, "%s", %d, %d, %d, %d)',
                  $form_state['values']['user'],
                  $form_state['values']['survey'], 1, $nid['loop'],
                  $q, drupal_substr($response, 0, strpos($response, '-')),
                  drupal_substr($response, strpos($response, '-') + 1), 1);
              }
            }
            else {
              db_query('
                INSERT INTO {deepsurvey_node_data_standard}
                (userid, deepsurvey_nid, instance, loops,
                question_nid, column_code, row_code, response)
                VALUES (%d, %d, %d, "%s", %d, %d, %d, %d)',
                $form_state['values']['user'],
                $form_state['values']['survey'], 1, $nid['loop'],
                $q, $col, $row, $response);
            }
          }
        }
      }
    }
  }
}

/**
 * Submission of question response.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_question_deepsurvey_item_form_submit($form, &$form_state) {

  deepsurvey_question_deepsurvey_item_form_submit_process($form, $form_state);

  // Set up next question.
  if ($form_state['clicked_button']['#value'] == t('forward')) {

    // Save question state.
    $previous_top = db_fetch_object(db_query('
      SELECT MAX(iorder) AS iorder FROM {deepsurvey_node_order}
      WHERE userid = %d AND deepsurvey_nid = %d',
      $form_state['values']['user'], $form_state['values']['survey']));

    db_query('
      INSERT INTO {deepsurvey_node_order}
      (userid, deepsurvey_nid, iorder, item_nid, mask_instance,
      scalemask_instance, questionmask_instance, substitution_instance)
      VALUES (%d, %d, %d, %d, %d, %d, %d, %d)',
      $form_state['values']['user'], $form_state['values']['survey'],
      $previous_top->iorder + 1, $form_state['values']['item'],
      $form_state['values']['mask_instance'],
      $form_state['values']['scalemask_instance'],
      $form_state['values']['questionmask_instance'],
      $form_state['values']['substitution_instance']);

    _deepsurvey_next_item($form_state['values']['parent'],
      $form_state['values']['parent_type'],
      $form_state['values']['item'],
      $form_state['values']['item_instance'],
      $form_state['values']['user']);
  }
  else {
    _deepsurvey_previous_item($form_state['values']['survey'],
      $form_state['values']['parent'],
      $form_state['values']['parent_type'],
      $form_state['values']['item'],
      $form_state['values']['item_instance'],
      $form_state['values']['user'],
      $form_state['values']['mask_instance'],
      $form_state['values']['scalemask_instance'],
      $form_state['values']['questionmask_instance'],
      $form_state['values']['substitution_instance']);
  }

}


/**
 * Validates the question responses.
 *
 * Validation of question response - generic bit which will also be called by
 * the node that displays multiple questions on one page.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_question_deepsurvey_item_form_validate_process($form, &$form_state) {

  foreach (element_children($form_state['values']['table']) as $q) {

    $x_count = array();
    $x_count_list = array();

    $y_count = array();
    $y_count_list = FALSE;

    // Get validation values from question.
    $question_validation = db_fetch_object(db_query('
      SELECT x_min, x_max, y_min, y_max, check_nid
      FROM {deepsurvey_question_node_properties}
      WHERE nid = %d', $q));

    // @todo This will call a custom function once the check module has been
    //   implemented
    // @code
    //   if (!is_null($question_validation->check_nid) {
    //     deepsurvey_check_basic($question_validation->check_nid,
    //       $form, $form_state);
    //   }
    // @endcode
    // Check each value.
    $nid = $form_state['values']['table'][$q];
    // Don't process table if no questions exist.
    if (is_array($nid['code'])) {
      foreach ($nid['code'] as $col => $columns) {
        foreach ($columns as $row => $response) {
          if ($response != '') {

            // Check for multipule responses in single response row or column.
            if (($form_state['values']['table'][$q]['type'][$col][$row]['row'] == DEEPSURVEY_QUESTION_SINGLE && $x_count_list[$row])
                || ($form_state['values']['table'][$q]['type'][$col][$row]['col'] == DEEPSURVEY_QUESTION_SINGLE && $y_count_list)
                && $form_state['values']['table'][$q]['code'][$col][$row]['#type'] != 'select') {
              form_set_error('table',
                t('Two responses entered in single response field'));
            }

            $x_count[$row] = TRUE;
            $x_count_list[$row] = TRUE;
            $y_count[$col] = TRUE;
            $y_count_list = TRUE;

            // Check for non - numeric in numeric field.
            if (($form_state['values']['table'][$q]['type'][$col][$row]['row'] == DEEPSURVEY_QUESTION_NUM
                || $form_state['values']['table'][$q]['type'][$col][$row]['col'] == DEEPSURVEY_QUESTION_NUM)
                && !is_numeric($response)) {
              form_set_error('table',
                t('Non-numeric value entered in numeric only field'));
            }

            // Reset list of heading boarder crossed.
            if ($form_state['values']['table'][$q]['boundary'][$row]['row'] != 0) {
              $y_count_list = FALSE;
            }
            if ($form_state['values']['table'][$q]['boundary'][$col]['col'] != 0) {
              $x_count_list = array();
            }

          }
        }
        $y_count_list = FALSE;
      }
    }

    if ($question_validation->x_max && count($x_count) > 1) {
      if (count($x_count) > $question_validation->x_max) {
        form_set_error('table',
          t('Too many responses entered, only @question_validation row@s should have a value',
          array('@question_validation' => $question_validation->x_max, '@s' => $question_validation->x_max == 1 ? '' : 's')));
      }
    }

    if (count($x_count) < $question_validation->x_min) {
      form_set_error('table',
        t('Please enter a value in at least @question_validation row@s',
        array('@question_validation' => $question_validation->x_min, '@s' => $question_validation->x_min == 1 ? '' : 's')));
    }

    if ($question_validation->y_max && count($y_count) > 1) {
      if (count($y_count) > $question_validation->y_max) {
        form_set_error('table',
          t('Too many responses entered, only @question_validation column@s should have a value',
          array('@question_validation' => $question_validation->y_max, '@s' => $question_validation->y_max == 1 ? '' : 's')));
      }
    }

    if (count($y_count) < $question_validation->y_min) {
      form_set_error('table',
        t('Please enter a value in at least @question_validation column@s',
        array('@question_validation' => $question_validation->y_min, '@s' => $question_validation->y_min == 1 ? '' : 's')));
    }

  }

}

/**
 * Validation of question response.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_question_deepsurvey_item_form_validate($form, &$form_state) {

  deepsurvey_question_deepsurvey_item_form_validate_process($form, $form_state);

}
