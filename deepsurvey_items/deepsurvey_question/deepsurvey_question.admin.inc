<?php

/**
 * @file
 * Administrator interface for Deep Survey Question module.
 *
 * Admin functions.
 *
 * Admin page related function used purely by the deep survey question module.
 */

// DEEP SURVEY QUESTION ADMIN.
/**
 * Displays information about a survey question node.
 *
 * Displays information about a survey question node and allows items to be
 * added to it.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the question node.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return string
 *   Formatted age displaying the items in the question and with controls for
 *   doing various things associated with each.
 */
function deepsurvey_question_edit_survey($deepsurvey_item_nid, $current_path, $last_page) {
  // Add code to determine if the node is a survey related one or not - go to
  // the node edit page if it is not a survey - add new db table to store this
  // info.
  $node_properties = db_fetch_object(db_query('
    SELECT type, title FROM {node}
    WHERE nid = %d', $deepsurvey_item_nid));

  if ($node_properties->type == 'deepsurvey_question') {
    drupal_set_title($node_properties->title);
    $rc = drupal_get_form('deepsurvey_edit_form',
      $deepsurvey_item_nid, $node_properties->type, $current_path, $last_page);

    $deepsurvey_title = db_fetch_object(db_query('
      SELECT title FROM {node}
      WHERE nid = %d', $deepsurvey_item_nid));

  }
  else {
    $rc = t('<p>none found</p>');
  }

  return $rc;
}


/**
 * Adds existing node to a survey question.
 *
 * Calles the form function to create the form for adding exising nodes to the
 * survey.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the question node.
 *
 * @param int $order
 *   The order of the item before where the existing items are to be added into
 *   the question, 0 if they are to be added to the beginning of the question.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return string
 *   Form.
 */
function deepsurvey_question_edit_add_existing_item($deepsurvey_item_nid, $order, $current_path, $last_page) {
  $rc = drupal_get_form('deepsurvey_question_add_existing_form',
    $deepsurvey_item_nid, $order, $current_path, $last_page);

  return $rc;
}

/**
 * Adds a new node to a survey question.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the question node.
 *
 * @param int $order
 *   The order of the item before where the existing items are to be added into
 *   the question, 0 if they are to be added to the beginning of the survey.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return string
 *   Form.
 */
function deepsurvey_question_edit_add_new_item($deepsurvey_item_nid, $order, $current_path, $last_page) {
  $rc = drupal_get_form('deepsurvey_question_add_new_form',
    $deepsurvey_item_nid, $order, $current_path, $last_page);

  return $rc;
}

// THEME FUNCTIONS.
// Remember to updated deepsurvey_theme() in deepsurvey_question.module.
/**
 * Theme the form for editing the survey question.
 */
function theme_deepsurvey_question_edit_table($form) {

  $headers = array(
    t('Order'),
    t('Id'),
    t('Name'),
    t('Link'),
    t('Display Order'),
    t('Unique Item'),
    t('Axis'),
    t('Response Type'),
    t('Reorder'),
    t('Remove'),
    t('Add new'),
    t('Add existing'),
  );
  $rows = array();

  if (!empty($form)) {
    foreach (element_children($form) as $nid) {

      $rows[] = array(
        'data' => array(
          check_plain(drupal_render($form[$nid]['order'])),
          drupal_render($form[$nid]['iid']),
          drupal_render($form[$nid]['name']),
          drupal_render($form[$nid]['link']),
          drupal_render($form[$nid]['display order radio']),
          drupal_render($form[$nid]['unique radio']),
          drupal_render($form[$nid]['select axis']),
          drupal_render($form[$nid]['select type']),
          drupal_render($form[$nid]['reorder box']),
          drupal_render($form[$nid]['remove box']),
          drupal_render($form[$nid]['add new']),
          drupal_render($form[$nid]['add existing']),
        ),
      );
    }

  }
  $table = theme('table', $headers, $rows);
  return $table . drupal_render($form);
}
