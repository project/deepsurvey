<?php

/**
 * @file
 * Utility functions for Deep Survey module.
 *
 * Utility functions.
 *
 * Various functions used by Deep Survey modules to handle common operations.
 */

/**
 * Recursive function used to insert a new item into a survey.
 *
 * @param int $new_order
 *   The order in which the new item has been inserted/update.
 *
 * @param int $container_nid
 *   The node id of the item into which the new item is being inserted.
 *
 * @param string $container_type
 *   The name of the table holding the order information.
 *
 * @return bool
 *   Only relevent for the first time the function is called,
 *   TRUE or FALSE to indicate if reordering of existing items has taken place.
 */
function _deepsurvey_check_new_order($new_order, $container_nid, $container_type) {

  if (db_fetch_object(db_query('SELECT item_order FROM {%s} WHERE deepsurvey_nid = %d AND item_order = %d', $container_type, $container_nid, $new_order))) {
    $rc = _deepsurvey_check_new_order($new_order + 1, $container_nid,
      $container_type);
    db_query('
      UPDATE {%s} SET item_order = %d
      WHERE item_order = %d AND deepsurvey_nid = %d',
      $container_type, $new_order + 1, $new_order, $container_nid);
    return TRUE;
  }
  else {
    return FALSE;
  }
}


/**
 * Reset the order values a survey so they are evenly distributed.
 *
 * @param string $deepsurvey_table
 *   The name of the table holding the order information.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the item which needs to be cleaned up.
 */
function _deepsurvey_reset_order($deepsurvey_table, $deepsurvey_item_nid) {

  $count_of_items = db_result(db_query('
    SELECT COUNT(*) FROM {%s}
    WHERE deepsurvey_nid = %d',
    $deepsurvey_table, $deepsurvey_item_nid));
  $result = db_query('
    SELECT item_order
    FROM {%s} WHERE deepsurvey_nid = %d
    ORDER BY item_order',
    $deepsurvey_table, $deepsurvey_item_nid);
  $loop_itr = 1;
  while ($nodes = db_fetch_object($result)) {
    db_query('
      UPDATE {%s} SET item_order = %d
      WHERE item_order = %d AND deepsurvey_nid=%d', $deepsurvey_table,
      (DEEPSURVEY_MAX_ITEMS / ($count_of_items + 1)) * $loop_itr,
      $nodes->item_order, $deepsurvey_item_nid);
    $loop_itr++;
  }
}

/**
 * Return a link to the previous page.
 *
 * @return string
 *   Link.
 */
function _deepsurvey_last_breadcrumb() {
  return preg_replace('/<a href=\"\/\?q=(.*?)\">(.*?)<\/a>/', "$1",
    end($_SESSION['deepsurvey_breadcrumb']));
}

/**
 * Advance the survey to the next question.
 *
 * @param int $parent_item
 *   The node id of the parent item in which the next item needs to be
 *   displayed.
 *
 * @param string $parent_type
 *   The type of the parent item (e.g. survey) in which the next item needs to
 *   be displayed.
 *
 * @param int $current_question
 *   The node id of the last question displayed.
 *
 * @param int $current_item_instance
 *   The instance the last question displayed.
 *
 * @param int $usr
 *   The user id of the current user.
 *
 * @return bool
 *   TRUE for success, FALSE if the current item is the last one.
 */
function _deepsurvey_next_item($parent_item, $parent_type, $current_question, $current_item_instance, $usr) {
  $rc = TRUE;
  $counter = 0;

  $item_instances = db_query('
    SELECT item_order
    FROM {%s_node_relationship}
    WHERE deepsurvey_nid = %d AND item_nid = %d',
    $parent_type, $parent_item, $current_question);
  do {
    $current_order = db_fetch_object($item_instances);
    $counter++;
  } while ($current_order && $current_item_instance > $counter);

  if ($current_order->item_order) {
    // @todo
    //   In order to handle random question presentation this will need to be
    //   updated to look at session variables rather then the database - will
    //   require editing of view functions to create session variables ******
    $next_item = db_fetch_object(db_query('
      SELECT item_nid
      FROM {%s_node_relationship}
      WHERE deepsurvey_nid = %d AND item_order IN(
      SELECT MIN(item_order)
      FROM {%s_node_relationship}
      WHERE item_order > %d AND deepsurvey_nid = %d)',
      $parent_type, $parent_item,
      $parent_type, $current_order->item_order, $parent_item));
  }

  if ($next_item->item_nid) {
    db_query('
      UPDATE {%s_node_state} SET current_question = %d
      WHERE userid = %d AND deepsurvey_nid = %d',
      $parent_type, $next_item->item_nid, $usr, $parent_item);
  }
  elseif (!is_null($_SESSION['deepsurvey_loops'][$parent_item])) {
    if (next($_SESSION['deepsurvey_loops'][$parent_item])) {
      // Go to next loop.
      // @todo
      //   In order to handle random question presentation this will need to be
      //   updated to look at session variables rather then the database - will
      //   require editing of view functions to create session variables ******
      $next_item = db_fetch_object(db_query('
        SELECT MIN(item_nid)
        FROM {%s_loops_relationship}
        WHERE deepsurvey_nid = %d', $parent_type, $parent_item));
      if ($next_item->item_nid) {
        db_query('
          UPDATE {%s_node_state} SET current_question = %d
          WHERE userid = %d AND deepsurvey_nid = %d',
          $parent_type, $next_item->item_nid, $usr, $parent_item);
      }
    }
    else {
      $_SESSION['deepsurvey_loop_end'][$parent_item] = TRUE;
    }
  }
  else {
    // Mark as finished.
    if ($current_question) {
      if ($parent_type == 'deepsurvey') {
        db_query('
          UPDATE {deepsurvey_node_state} SET current_question = %d
          WHERE userid = %d AND deepsurvey_nid = %d', 0, $usr, $parent_item);
      }
    }
    $rc = FALSE;
  }

  return $rc;
}

/**
 * Retract the survey to the previous question.
 *
 * @param int $survey
 *   The node id of the current survey.
 *
 * @param int $parent_item
 *   The node id of the parent item in which the next item needs to be
 *   displayed.
 *
 * @param string $parent_type
 *   The type of the parent item (e.g. survey) in which the next item needs to
 *   be displayed.
 *
 * @param int $current_question
 *   The node id of the last question displayed.
 *
 * @param int $current_item_instance
 *   The instance the last question displayed.
 *
 * @param int $usr
 *   The user id of the current user.
 *
 * @param int $current_mask_instance
 *   instance of the current mask, used to determine the new mask.
 *
 * @param int $current_scalemask_instance
 *   instance of the current scalemask, used to determine the new scalemask.
 *
 * @param int $current_questionmask_instance
 *   instance of the current scalemask, used to determine the new scalemask.
 *
 * @param int $current_substitution_instance
 *   instance of the current substitution values, used to determine the new
 *   substitution values.
 *
 * @return bool
 *   TRUE for success, FALSE if the current item is the last one
 */
function _deepsurvey_previous_item($survey, $parent_item, $parent_type, $current_question, $current_item_instance, $usr, $current_mask_instance, $current_scalemask_instance, $current_questionmask_instance, $current_substitution_instance) {
  $rc = TRUE;
  $counter = 0;

  $current_item_instances = db_query('
    SELECT item_order
    FROM {%s_node_relationship}
    WHERE deepsurvey_nid = %d AND item_nid = %d',
    $parent_type, $parent_item, $current_question);
  do {
    $current_order = db_fetch_object($current_item_instances);
    $counter++;
  } while ($current_item_instance > $counter);

  // Going backwards to need to take determine previous order.
  if ($current_order->item_order) {
    $next_item = db_fetch_object(db_query('
    SELECT item_nid
    FROM {%s_node_relationship}
    WHERE deepsurvey_nid = %d AND item_order < %d AND item_nid IN(
    SELECT item_nid
    FROM {deepsurvey_node_order}
    WHERE deepsurvey_nid = %d AND userid = %d AND iorder IN(
    SELECT MAX(iorder) as iorder
    FROM {deepsurvey_node_order}
    WHERE deepsurvey_nid = %d AND userid = %d))',
    $parent_type, $parent_item, $current_order->item_order,
    $survey, $usr, $survey, $usr));
  }

  if ($next_item->item_nid) {
    // Get the relevent attribute from the global state list and set filters.
    $next_attributes = db_fetch_object(db_query('
      SELECT MAX(iorder) as iorder, mask_instance, scalemask_instance,
      questionmask_instance, substitution_instance
      FROM {%s_node_order}
      WHERE deepsurvey_nid = %d AND userid = %d
      GROUP BY userid', $parent_type, $survey, $usr));
    if ($next_attributes->mask_instance != $current_mask_instance || $next_attributes->scalemask_instance != $current_scalemask_instance || $next_attributes->questionmask_instance != $current_questionmask_instance || $next_attributes->substitution_instance != $current_substitution_instance) {

      db_query('
        UPDATE {deepsurvey_node_state_global}
        SET mask_instance = %d, scalemask_instance = %d,
        questionmask_instance = %d, substitution_instance = %d
        WHERE userid = %d AND deepsurvey_nid = %d',
        $current_mask_instance, $current_scalemask_instance,
        $current_questionmask_instance, $current_substitution_instance, $usr,
        $parent_item);

      // Pop rows from the state tables (including higher ordered rows).
      if ($next_attributes->mask_instance != $current_mask_instance) {
        db_query('
          DELETE FROM {deepsurvey_node_mask}
          WHERE deepsurvey_nid = %d AND userid = %d AND instance > %d',
          $survey, $usr, $next_attributes->mask_instance);
      }
      if ($next_attributes->scalemask_instance != $current_scalemask_instance) {
        db_query('
          DELETE FROM {deepsurvey_node_scalemask}
          WHERE deepsurvey_nid = %d AND userid = %d AND instance > %d',
          $survey, $usr, $next_attributes->scalemask_instance);
      }
      if ($next_attributes->questionmask_instance != $current_questionmask_instance) {
        db_query('
          DELETE FROM {deepsurvey_node_questionmask}
          WHERE deepsurvey_nid = %d AND userid = %d AND instance > %d',
          $survey, $usr, $next_attributes->questionmask_instance);
      }
      if ($next_attributes->substitution_instance != $current_substitution_instance) {
        db_query('
          DELETE FROM {deepsurvey_node_substitutions}
          WHERE deepsurvey_nid = %d AND userid = %d AND instance > %d',
          $survey, $usr, $next_attributes->substitution_instance);
      }
    }

    db_query('
      DELETE FROM {%s_node_order}
      WHERE deepsurvey_nid = %d AND userid = %d AND iorder >= %d',
      $parent_type, $survey, $usr, $next_attributes->iorder);

    db_query('
      UPDATE {%s_node_state} SET current_question = %d
      WHERE userid = %d AND deepsurvey_nid = %d',
      $parent_type, $next_item->item_nid, $usr, $parent_item);

    if (!is_null($_SESSION['deepsurvey_loops'][$next_item->item_nid])) {
      $_SESSION['deepsurvey_loop_end'][$next_item->item_nid] = FALSE;
    }
  }
  else {
    $rc = FALSE;
  }

  return $rc;
}
