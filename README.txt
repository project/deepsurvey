
Overview
--------
The deep survey module is a framework which allows you to create surveys for 
your visitors. Refer to the Deep Survey Manual for further information. 


Requirements
------------
Drupal 6.x
PHP 5.1

Installation
------------
Please refer to the INSTALL file for installation directions.
