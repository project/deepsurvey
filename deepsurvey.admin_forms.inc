<?php

/**
 * @file
 * Administrator form functions for Deep Survey module.
 *
 * Admin form functions.
 *
 * Contains all the functionality of the forms from the admin pages that need
 * global scope.
 */

/**
 * Builds the form to display and edit the survey items.
 *
 * Build the form that allows the the survey nodes (questions and so forth) to
 * be added and removed from the survey and other "container" type nodes -
 * calling node specific functions.
 * These functions are of the format <node type>_deepsurvey_edit_form.
 *
 * @param array $form_state
 *   Standard parameter for form calls.
 *
 * @param int $deepsurvey_item_nid
 *   Node id of the item displaying it's child items.
 *
 * @param string $node_type
 *   The type of the item displaying it's child items.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form.
 */
function deepsurvey_edit_form($form_state, $deepsurvey_item_nid, $node_type, $current_path, $last_page) {

  $form = call_user_func_array($node_type . '_deepsurvey_edit_form',
    array($deepsurvey_item_nid, $node_type, $current_path, $last_page));

  // Create items to identify the form when it is submitted.
  $form['nid'] = array(
    '#type' => 'value',
    '#value' => $deepsurvey_item_nid,
  );
  $form['ntype'] = array(
    '#type' => 'value',
    '#value' => $node_type,
  );

  return $form;
}

/**
 * Submit function for survey edit form - calls function specific for that type.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_edit_form_submit($form, &$form_state) {
  $node_type = db_fetch_object(db_query('
    SELECT type FROM {node}
    WHERE nid = %d', $form_state['values']['nid']));
  call_user_func_array($node_type->type . '_deepsurvey_edit_form_submit',
    array($form, &$form_state));
}

/**
 * Validate function for survey edit form.
 *
 * Calls validate function specific for that type.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_edit_form_validate($form, &$form_state) {
  $node_type = db_fetch_object(db_query('
    SELECT type FROM {node}
    WHERE nid = %d', $form_state['values']['nid']));
  call_user_func_array($node_type->type . '_deepsurvey_edit_form_validate',
    array($form, &$form_state));
}

/**
 * This is the surveys own implementation of the node specific edit function.
 *
 * @param int $deepsurvey_item_nid
 *   Node id of the item being processed.
 *
 * @param string $node_type
 *   The type of the item being processed.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form.
 */
function deepsurvey_deepsurvey_edit_form($deepsurvey_item_nid, $node_type, $current_path, $last_page) {
  // Create a variable which counts the loops and hence the order of the items.
  $order = 0;

  $form['start'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
  );
  $form['start']['new'] = array(
    '#value' => l(t('add a new item to the start of the survey'),
      $current_path . '/new/0'),
    '#theme' => 'item',
  );
  $form['start']['existing'] = array(
    '#value' => l(t('add existing items to the start of the survey'),
      $current_path . '/existing/0'),
    '#theme' => 'item',
  );
  $form['table'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#theme' => 'deepsurvey_edit_table',
  );

  $result = db_query(db_rewrite_sql('
    SELECT r.item_order, r.item_nid, n.title
    FROM {deepsurvey_node_relationship} r
    INNER JOIN {node} n ON r.item_nid = n.nid
    WHERE r.deepsurvey_nid = %d ORDER BY r.item_order'), $deepsurvey_item_nid);
  while ($nodes = db_fetch_object($result)) {
    $order++;

    $form['table'][$nodes->item_order]['order'] = array(
      '#value' => $order,
    );
    $form['table'][$nodes->item_order]['iid'] = array(
      '#value' => $nodes->item_nid,
    );
    $form['table'][$nodes->item_order]['itid'] = array(
      '#type' => 'value',
      '#value' => $nodes->item_nid,
    );
    $form['table'][$nodes->item_order]['name'] = array(
      '#value' => $nodes->title,
    );
    $form['table'][$nodes->item_order]['link'] = array(
      '#value' =>
      l(t('edit'), $current_path . '/' . $nodes->item_nid),
    );
    $form['table'][$nodes->item_order]['reorder box'] = array(
      '#type' => 'textfield',
      '#size' => 4,
      '#maxlength' => 4,
      '#parents' => array('table', t($nodes->item_order), 'reorder box'),
    );
    $form['table'][$nodes->item_order]['remove box'] = array(
      '#type' => 'checkbox',
      '#parents' => array('table', t($nodes->item_order), 'remove box'),
    );
    $form['table'][$nodes->item_order]['add new'] = array(
      '#value' =>
      l(t('add a new item here'),
      $current_path . '/new/' . $nodes->item_order),
    );
    $form['table'][$nodes->item_order]['add existing'] = array(
      '#value' =>
      l(t('add existing items here'),
      $current_path . '/existing/' . $nodes->item_order),
    );
  };

  $form['last page'] = array(
    '#type' => 'value',
    '#value' => $last_page,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Done'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Back'),
  );

  return $form;
}

/**
 * Calls the module specific edit form submit function.
 *
 * This is the surveys own implementation of the node specific edit function -
 * form submission.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_deepsurvey_edit_form_submit($form, &$form_state) {
  // Removed the processing of the form data from this function so that other
  // functions can also use it if need be (i.e. form like survey nodes).
  deepsurvey_edit_process_standard($form, $form_state,
    'deepsurvey_node_properties');
}

/**
 * Calls the module specific edit form validation function.
 *
 * This is the surveys own implementation of the node specific edit function -
 * form validation.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_deepsurvey_edit_form_validate($form, &$form_state) {
  // Removed the processing of the form data from this function so that other
  // functions can also use it if need be (i.e. form like survey nodes).
  deepsurvey_edit_validate_standard($form, $form_state);
}

/**
 * Processes the edit form.
 *
 * This is the main function where the processing of the edit form takes place,
 * it has been disentangled from the node specific function so other deep
 * survey modules can use it.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 *
 * @param string $node_table_properties
 *   String containing the name of the particular node properties table for
 *   this type.
 */
function deepsurvey_edit_process_standard($form, &$form_state, $node_table_properties) {

  if ($form_state['clicked_button']['#value'] == t('Done')) {
    // Flag any reorder boxes being set.
    $local_flag = FALSE;
    $reset_flag = FALSE;
    $local_order = 1;
    $local_prev = 0;
    $local_offset = 0;
    $new_id = 0;
    // Item id of the reordered items.
    $new_order = array();
    // Relationship id of the items not being reordered.
    $old_order = array();
    reset($form_state['values']['table']);

    while (list($key, $form_order) = each($form_state['values']['table'])) {
      if ($form_state['values']['table'][$key]['remove box'] == TRUE) {
        db_query('
          DELETE FROM {deepsurvey_node_relationship}
          WHERE item_order = %d AND deepsurvey_nid = %d',
          $key, $form_state['values']['nid']);
      }
      elseif ($form_state['values']['table'][$key]['reorder box'] != '') {
        // Remove item from current order.
        db_query('
          DELETE FROM {deepsurvey_node_relationship}
          WHERE item_order = %d AND deepsurvey_nid = %d',
          $key, $form_state['values']['nid']);
        $new_order[(int) $form_state['values']['table'][$key]['reorder box']]
          = $form_state['values']['table'][$key]['itid'];
        $local_flag = TRUE;
      }
      else {
        $old_order[$local_order] = $key;
        $local_order++;
      }
    }

    // Arrays of the survey item order have been created, one with the order of
    // the items whose order was not changed and one with the changed order
    // items, compare the two and make changes to the database where necessary.
    $local_order = 1;
    if ($local_flag) {
      while ($old_order[$local_order - $local_offset] != FALSE || $new_order[$local_order] != FALSE) {
        if ($old_order[$local_order - $local_offset] != FALSE && $new_order[$local_order] != FALSE) {
          if ($local_prev >= $old_order[$local_order - $local_offset] + 1) {
            $loop_prev = $local_prev;
            $looper = 0;
            while ($loop_prev >= $old_order[$local_order - $local_offset + $looper] + 1) {
              db_query('
                UPDATE {deepsurvey_node_relationship} SET item_order = %d
                WHERE item_order = %d AND deepsurvey_nid = %d',
                $old_order[$local_order - $local_offset + $looper],
                $old_order[$local_order - $local_offset + $looper] + 1,
                $form_state['values']['nid']);
              $old_order[$local_order - $local_offset + $looper]++;
              $loop_prev = $old_order[$local_order - $local_offset + $looper];
              $looper++;
              $reset_flag = TRUE;
            }
          }
          $new_id = floor($local_prev +
            (($old_order[$local_order - $local_offset] - $local_prev) / 2));
          db_query('
            INSERT INTO {deepsurvey_node_relationship}
            (deepsurvey_nid, item_nid, item_order) VALUES (%d, %d, %d)',
            $form_state['values']['nid'], $new_order[$local_order], $new_id);
          $local_offset++;
          $local_prev = $new_id;
        }
        elseif ($old_order[$local_order - $local_offset] == FALSE && $new_order[$local_order] != FALSE) {
          if ($local_prev < DEEPSURVEY_MAX_ITEMS) {
            $new_id = floor($local_prev +
              ((DEEPSURVEY_MAX_ITEMS - $local_prev) / 2));
            db_query('
              INSERT INTO {deepsurvey_node_relationship}
              (deepsurvey_nid, item_nid, item_order)
              VALUES (%d, %d, %d)',
              $form_state['values']['nid'], $new_order[$local_order], $new_id);
            $local_prev = $new_id;
          }
          else {
            $reset_flag = TRUE;
          }
        }
        else {
          $local_prev = $old_order[$local_order - $local_offset];
        }
        $local_order++;
      }

      if ($reset_flag) {
        _deepsurvey_reset_order('deepsurvey_node_relationship',
          $form_state['values']['nid']);
      }

    }
    menu_rebuild();
    drupal_set_message(t('Survey Updated'));
  }
  else {
    $form_state['redirect'] = $form_state['values']['last page'];
  }
}

/**
 * Validation function for the standard edit form processing function.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_edit_validate_standard($form, &$form_state) {

  if ($form_state['clicked_button']['#value'] == t('Done')) {
    $number_of_items = 0;
    $highest_reorder = 0;
    // Item id of the reordered items.
    $reorder = array();

    while (list($key, $form_order) = each($form_state['values']['table'])) {

      if ($form_state['values']['table'][$key]['remove box'] == TRUE && $form_state['values']['table'][$key]['reorder box'] != '') {
        form_set_error('table][' . $key . '][remove box',
          t('You can not reorder an item marked for deletion'));
      }

      if ($form_state['values']['table'][$key]['reorder box'] < 0 || $form_state['values']['table'][$key]['reorder box'] === 0) {
        form_set_error('table][' . $key . '][reorder box',
          t('reorder value 0 or less'));
      }
      elseif ($form_state['values']['table'][$key]['reorder box'] != '') {
        if ($reorder[$form_state['values']['table'][$key]['reorder box']] > 0) {
          form_set_error('table][' . $key . '][reorder box',
          t('duplicate reorder value'));
        }
        else {
          $reorder[$form_state['values']['table'][$key]['reorder box']] = $key;
        }
      }

      if ($form_state['values']['table'][$key]['reorder box'] > $highest_reorder) {
        $highest_reorder = $form_state['values']['table'][$key]['reorder box'];
      }

      $number_of_items++;
    }

    if ($highest_reorder > $number_of_items) {
      form_set_error('table][' . $reorder[$highest_reorder] . '][reorder box',
        t('reorder value higher than the number of items'));
    }

  }
}

/**
 * Build the form that allows the user to add existing nodes to a survey.
 *
 * @param array $form_state
 *   The form state array.
 *
 * @param int $deepsurvey_item_nid
 *   If $order is 0 then this is the node id of the survey object the node(s)
 *   is being added to, otherwise it is the item/survey relationship that the
 *   nodes are to be inserted after.
 *
 * @param int $order
 *   The order of the node before the new node(s) or the value of the next
 *   relationship id if order isn't 0.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form.
 */
function deepsurvey_add_existing_form($form_state, $deepsurvey_item_nid, $order, $current_path, $last_page) {

  // @todo I will eventually add a way to filter this list.
  $deepsurvey_title = db_fetch_object(db_query('
    SELECT title
    FROM {node}
    WHERE nid = %d', $deepsurvey_item_nid));

  drupal_set_title(t("Add existing items to @name",
    array('@name' => check_plain($deepsurvey_title->title))));

  // Store parameters needed when processing the form.
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $deepsurvey_item_nid,
  );
  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );

  $form['table'] = array(
    '#type' => 'fieldset',
    '#tree' => TRUE,
    '#theme' => 'deepsurvey_add_existing_questions_table',
  );

  $result = db_query('SELECT n.nid, n.title, n.created
          FROM {node} n
          INNER JOIN {deepsurvey_node_registration} s
          ON s.type = n.type
          WHERE n.type <> "deepsurvey"
          AND NOT n.nid IN(
          SELECT item_nid
          FROM {deepsurvey_node_relationship}
          WHERE deepsurvey_nid = %d)', $deepsurvey_item_nid);
  // @todo Will be added later.
  //   INNER JOIN {users} u
  //   ON u.uid = n.uid');
  //   WHERE n.type = $filter');
  while ($nodes = db_fetch_object($result)) {
    $form['table'][$nodes->nid]['name'] = array(
      '#value' => t($nodes->title),
    );
    $form['table'][$nodes->nid]['created'] = array(
      '#value' => t($nodes->created),
    );
    $form['table'][$nodes->nid]['select box'] = array(
      '#type' => 'checkbox',
    );
  };

  $form['last page'] = array(
    '#type' => 'value',
    '#value' => $last_page . '/' . $deepsurvey_item_nid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Done'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Deepsurvey_add_existing submit function.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_add_existing_form_submit($form, &$form_state) {

  if ($form_state['clicked_button']['#value'] == t('Done')) {
    $node_properties = db_fetch_object(db_query('
      SELECT type, title
      FROM {node}
      WHERE nid = %d', $form_state['values']['rid']));

    if (db_fetch_object(db_query('SELECT type FROM {deepsurvey_node_registration} WHERE type = "%s"', $node_properties->type))) {
      call_user_func_array(
        $node_properties->type . '_deepsurvey_add_existing_form_submit',
        array($form, &$form_state));
    }
  }
  $form_state['redirect'] = $form_state['values']['last page'];
}

/**
 * Deepsurvey_add_existing generic submit function.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_deepsurvey_add_existing_form_submit($form, &$form_state) {
  // Removed the processing of the form data from this function so that other
  // functions can also use it if need be (i.e. form-like survey nodes).
  deepsurvey_add_existing_process_standard($form, $form_state,
    'deepsurvey_node_properties');
}

/**
 * Processes the add existing form.
 *
 * This is the main function where the processing of the add existing form takes
 * place, it has been disentangled from the node specific function so other deep
 * survey modules can use it.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 *
 * @param string $node_table_properties
 *   String containing the name of the particular node properties table for
 *   this type.
 */
function deepsurvey_add_existing_process_standard($form, &$form_state, $node_table_properties) {

  $local_last = $form_state['values']['order'];
  $next_item_in_order = db_fetch_object(db_query('
    SELECT item_order
    FROM {deepsurvey_node_relationship}
    WHERE deepsurvey_nid = %d AND item_order>%d
    ORDER BY item_order',
    $form_state['values']['rid'], $form_state['values']['order']));
  $local_next = $next_item_in_order->item_order;
  $reset_flag = FALSE;

  // In case survey has no items.
  if (!$local_next) {
    $local_next = DEEPSURVEY_MAX_ITEMS;
  }

  foreach (element_children($form_state['values']['table']) as $nid) {
    if ($form_state['values']['table'][$nid]['select box'] == TRUE) {
      if ($local_last >= $local_next + 1) {
        $loop_prev = $local_last;
        $loop_next = $local_next;
        $local_next++;
        while ($loop_prev >= $loop_next + 1) {
          db_query('
            UPDATE {deepsurvey_node_relationship}
            SET item_order = %d
            WHERE item_order = %d AND deepsurvey_nid = %d',
            $loop_next, $loop_next + 1, $nid);
          $loop_prev = $loop_next + 1;
          $loop_next = db_fetch_object(db_query('
            SELECT item_order
            FROM {deepsurvey_node_relationship}
            WHERE deepsurvey_nid = %d AND item_order>%d
            ORDER BY item_order',
            $form_state['values']['rid'], $loop_prev));
          $reset_flag = TRUE;
        }
      }
      $new_id = floor($local_last + (($local_next - $local_last) / 2));
      db_query('
        INSERT INTO {deepsurvey_node_relationship}
        (deepsurvey_nid, item_nid, item_order)
        VALUES (%d, %d, %d)', $form_state['values']['rid'], $nid, $new_id);
      $local_last = $new_id;
    }
  }

  if ($reset_flag) {
    _deepsurvey_reset_order('deepsurvey_node_relationship',
      $form_state['values']['nid']);
  }

  drupal_set_message(t('New items have been added to the survey'));
  menu_rebuild();
}

/**
 * Builds the form for adding new items to the survey.
 *
 * Build the form that allows the user to choose which type of node to create
 * and add to a survey.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 *
 * @param int $deepsurvey_item_nid
 *   If $order is 0 then this is the node id of the survey object the node is
 *   being added to, otherwise is it the it is the item/survey relationship
 *   that the node is to be inserted after.
 *
 * @param int $order
 *   The order of the node before the new node(s) or the value of the next
 *   relationship id if order isn't 0.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return array
 *   Form.
 */
function deepsurvey_add_new_form($form_state, $deepsurvey_item_nid, $order, $current_path, $last_page) {

  $deepsurvey_title = db_fetch_object(db_query('
    SELECT title
    FROM {node}
    WHERE nid = %d', $deepsurvey_item_nid));

  drupal_set_title(t("Add new item to @name",
    array('@name' => check_plain($deepsurvey_title->title))));

  // Store parameters needed when processing the form.
  $form['rid'] = array(
    '#type' => 'value',
    '#value' => $deepsurvey_item_nid,
  );
  $form['order'] = array(
    '#type' => 'value',
    '#value' => $order,
  );

  // Get list of node types from the database.
  $result = db_query('
          SELECT n.type, n.name
          FROM {node_type} n
          INNER JOIN {deepsurvey_node_registration} s
          ON n.type = s.type');
  while ($types = db_fetch_object($result)) {
    if ($types->type != 'deepsurvey') {
      $type_array[str_ireplace('_', '-', $types->type)] = $types->name;
    }
  }
  $form['add_type'] = array(
    '#type' => 'select',
    '#title' => 'New Item Type',
    '#options' => $type_array,
  );

  $form['last page'] = array(
    '#type' => 'value',
    '#value' => $last_page . '/' . $deepsurvey_item_nid,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Done'),
  );
  $form['cancel'] = array(
    '#type' => 'submit',
    '#value' => t('Cancel'),
  );

  return $form;
}

/**
 * Deepsurvey_add_new submit function.
 *
 * @param array $form
 *   Standard form submit function parameter.
 *
 * @param array $form_state
 *   Standard form submit function parameter.
 */
function deepsurvey_add_new_form_submit($form, &$form_state) {
  if ($form_state['clicked_button']['#value'] == t('Done')) {
    if ($form_state['values']['add_type']) {
      $_SESSION['deepsurvey_new_rid'] = $form_state['values']['rid'];
      $_SESSION['deepsurvey_new_order'] = $form_state['values']['order'];
      $_SESSION['deepsurvey_new_table'] = 'deepsurvey_node_relationship';
      $_SESSION['deepsurvey_last_page'] = $form_state['values']['last page'];
      $form_state['redirect'] = 'node/add/' . $form_state['values']['add_type'];
    }
  }
  else {
    $form_state['redirect'] = $form_state['values']['last page'];
  }

}

/**
 * Takes a time element and prepares to send it to form_date().
 *
 * Taken from quiz.
 *
 * @param array|NULL $time
 *   The time to be turned into an array. This can be:
 *   - A timestamp when from the database.
 *   - An array (day, month, year) when previewing.
 *   - NULL for new nodes.
 *
 * @return array
 *   An array for form_date (day, month, year).
 */
function _deepsurvey_form_prepare_date($time = '', $offset = 0) {
  // If this is empty, get the current time.
  if ($time == '') {
    $time = time();
    $time = strtotime("+$offset days", $time);
  }
  // If we are previewing, $time will be an array so just pass it through...
  $time_array = array();
  if (is_array($time)) {
    $time_array = $time;
  }
  // ...otherwise build the array from the timestamp.
  elseif (is_numeric($time)) {
    $time_array = array(
      'day' => _deepsurvey_date('j', $time),
      'month' => _deepsurvey_date('n', $time),
      'year' => _deepsurvey_date('Y', $time),
    );
  }
  return $time_array;
}

/**
 * Removes deleted deepsurvey nodes.
 *
 * Remove deleted deepsurvey content type node from tables in which they are
 * referenced. Called from other deepsurvey modules hook_delete.
 *
 * @param int $deleted_node
 *   The node to be removed from tables.
 *
 * @return bool
 *   return false if any problems were encountered otherwise true
 */
function deepsurvey_delete_content_type_node($deleted_node) {
  $rc = db_query('
    DELETE FROM {deepsurvey_node_relationship}
    WHERE item_nid = %d', $deleted_node);

  if ($rc) {
    return TRUE;
  }
  else {
    return FALSE;
  }

}

/**
 * Build the menu items.
 *
 * @param array $menu_items
 *   The standard menu array from hook_menu,
 *   however each item within it will also have to have
 *   a "current_node" value for the current node
 *   and a "child_nodes" array for the child nodes.
 * @param sting $module_name
 *   The name of the module calling the function.
 *
 * @return array
 *   The menu array from hook_menu in the correct deep survey hierarchy.
 */
function _deepsurvey_build_menu($menu_items, $module_name) {

  // Set variables to hold all the menu items for each time the function is
  // called, this is an array of "current_item" values.
  static $deepsurvey_menu_items;
  static $deepsurvey_menu_items_survey;
  static $deepsurvey_menu_modules;

  $menu_out = array();

  // Check if the function has been called for this module before, and if so
  // assume it is a new menu build and reset the static variable - also set
  // variable is not set.
  if ((!is_null($deepsurvey_menu_modules[$module_name])) || is_null($deepsurvey_menu_items)) {
    $deepsurvey_menu_items = array();
    $deepsurvey_menu_modules = array();
  }

  $deepsurvey_menu_modules[$module_name] = 1;

  if (!is_null($menu_items)) {
    if ($module_name == 'deepsurvey') {
      // Build all saved menus.
      foreach ($menu_items as $current_key => $current_item) {
        $menu_out[$current_key] = array_diff_key($menu_items[$current_key],
          array('child_nodes' => 0));
        $menu_out[$current_key]['page arguments'][] = $current_key;
        $menu_out[$current_key]['page arguments'][] = 'admin/deepsurvey';
        if ($current_item['child_nodes']) {
          foreach ($current_item['child_nodes'] as $next_item) {
            $menu_out = array_merge($menu_out,
              _deepsurvey_build_menu_init($current_key,
              $next_item, $deepsurvey_menu_items));
          }
        }
      }
      $deepsurvey_menu_items_survey = $menu_items;
    }
    elseif (!is_null($deepsurvey_menu_modules['deepsurvey'])) {
      // Survey module has already called this function so build any new menus
      // immediately.
      foreach ($menu_items as $current_key => $current_item) {
        $deepsurvey_menu_items[$current_item['current_node']][$current_key]
          = $menu_items[$current_key];
        $menu_items_temp[$current_item['current_node']][$current_key]
          = $menu_items[$current_key];
      }
      foreach ($deepsurvey_menu_items_survey as $current_key_survey => $current_item_survey) {
        if ($current_item_survey['child_nodes']) {
          foreach ($current_item_survey['child_nodes'] as $next_item) {
            $menu_out = array_merge($menu_out,
              _deepsurvey_build_menu_init($current_key_survey, $next_item,
              $deepsurvey_menu_items, $menu_items_temp));
          }
        }
      }
    }
    else {
      // Wait for deepsurvey module before returning anything.
      foreach ($menu_items as $current_key => $current_item) {
        $deepsurvey_menu_items[$current_item['current_node']][$current_key]
          = $menu_items[$current_key];
      }
      $menu_out = NULL;
    }
  }
  else {
    $menu_out = NULL;
  }

  return $menu_out;

}

/**
 * Build the menu items - recursive function called on initial build.
 *
 * @param sting $menu_path
 *   The current path to be prefixed to the menu items key.
 * @param int $current_node
 *   The node which the current menu item relates to.
 * @param array $menu_items_pointer
 *   The menu data structer from whcih the data is being pulled.
 * @param array $current_menu
 *   An array of the menu items to be returned if not all of them are to be
 *   included.
 *
 * @return array
 *   Menu array.
 */
function _deepsurvey_build_menu_init($menu_path, $current_node, &$menu_items_pointer, $current_menu = array('all' => 1)) {
  $menu_out = array();

  if (!is_null($menu_items_pointer[$current_node])) {
    foreach ($menu_items_pointer[$current_node] as $item_key => $current_item) {

      $current_path = $menu_path . $item_key;

      // Make sure path isn't too long for drupal to handle.
      if (substr_count($current_path, '/') > DEEPSURVEY_DEFAULT_MENU_DEPTH) {
        $menu_depth_length
          = substr_count($current_path, '/') - DEEPSURVEY_DEFAULT_MENU_DEPTH;
        $current_path
          = 'admin/deepsurvey/' . str_replace('/', '_',
          drupal_substr($current_path, 17), $menu_depth_length);
      }

      if (!is_null($current_menu[$menu_item_pointer[$current_node]] || $current_menu['all'] == 1)) {
        $menu_out[$current_path]
          = array_diff_key($current_item,
          array('child_nodes' => 0, 'current_node' => 0));

        // Cycle through existing page arguments to check for wildcard
        // parameters and increment the number appropriately.
        foreach ($menu_out[$current_path]['page arguments'] as $pkey => $parameter) {
          if (!is_string($parameter)) {
            $menu_out[$current_path]['page arguments'][$pkey]
              = $parameter + substr_count($menu_path, '/');
          }
        }

        if (is_array($menu_out[$current_path]['page arguments'])) {
          $menu_out[$current_path]['page arguments'][] = $current_path;
          // For returning to the previous page.
          $menu_out[$current_path]['page arguments'][] = $menu_path;
        }
      }
      if (!is_null($current_item['child_nodes'])) {
        foreach ($current_item['child_nodes'] as $next_item) {
          $menu_out = array_merge($menu_out,
            _deepsurvey_build_menu_init($current_path, $next_item,
            $menu_items_pointer));
        }
      }
    }
  }

  return $menu_out;
}
