<?php

/**
 * @file
 * Handles the start and end times in a node form submission.
 * Date and time routines for use with survey module.
 * - stolen from quiz module.
 * - All references to event variables should be optional.
 * - Takes the array from form_date() and turns it into a timestamp.
 * - Adjusts times for time zone offsets.
 * - Adapted from event.module.
 */

/**
 * Translate the form date.
 *
 * @param object $node
 *   The submitted node with form data.
 *
 * @param string $date
 *   The name of the date ('survey_open' or 'survey_close') to
 *   translate.
 */
function deepsurvey_translate_form_date(&$node, $date) {
  $prefix = $node->$date;
  // If we have all the parameters, re-calculate $node->event_$date .
  if (isset($prefix['year']) && isset($prefix['month']) && isset($prefix['day'])) {
    // Build a timestamp based on the date supplied and the configured timezone.
    $node->$date = _deepsurvey_mktime(0, 0, 0, $prefix['month'], $prefix['day'], $prefix['year'], 0);
  }
  else {
    form_set_error('survey_open', t('Please supply a valid date.'));
  }
}

/**
 * Formats local time values to GMT timestamp using time zone offset supplied.
 *
 * All time values in the database are GMT and translated here prior to
 * insertion.
 *
 * Time zone settings are applied in the following order:
 * 1. If supplied, time zone offset is applied.
 * 2. If user time zones are enabled, user time zone offset is applied.
 * 3. If neither 1 nor 2 apply, the site time zone offset is applied.
 *
 * @param int $hour
 *   The number of the hour relevant to the start of the day determined by
 *   month, day and year. Negative values reference the hour before midnight
 *   of the day in question. Values greater than 23 reference the appropriate
 *   hour in the following day(s).
 *
 * @param int $minute
 *   The number of the minute relevant to the start of the hour.
 *
 * @param int $second
 *   The number of seconds relevant to the start of the minute.
 *
 * @param int $month
 *   The number of the month relevant to the end of the previous year.
 *
 * @param int $day
 *   The number of the day relevant to the end of the previous month.
 *
 * @param int $year
 *   The year.
 *
 * @param int|NULL $offset
 *   Time zone offset to apply to the timestamp.
 *
 * @return gmdate()
 *   formatted date value.
 */
function _deepsurvey_mktime($hour, $minute, $second, $month, $day, $year, $offset = NULL) {
  global $user;
  $timestamp = gmmktime($hour, $minute, $second, $month, $day, $year);
  if (variable_get('configurable_timezones', 1) && $user->uid && drupal_strlen($user->timezone)) {
    return $timestamp - $user->timezone;
  }
  else {
    return $timestamp - variable_get('date_default_timezone', 0);
  }
}

/**
 * Formats a GMT timestamp to local date values using time zone offset supplied.
 *
 * All timestamp values in event nodes are GMT and translated for display here.
 * Pulled from event.
 *
 * Time zone settings are applied in the following order:
 * 1. If supplied, time zone offset is applied.
 * 2. If user time zones are enabled, user time zone offset is applied.
 * 3. If neither 1 nor 2 apply, the site time zone offset is applied.
 *
 * @param string $format
 *   The date() format to apply to the timestamp.
 *
 * @param int $timestamp
 *   The GMT timestamp value.
 *
 * @param int|NULL $offset
 *   Time zone offset to apply to the timestamp.
 *
 * @return gmdate()
 *   formatted date value.
 */
function _deepsurvey_date($format, $timestamp, $offset = NULL) {
  global $user;

  if (isset($offset)) {
    $timestamp += $offset;
  }
  elseif (variable_get('configurable_timezones', 1) && $user->uid && drupal_strlen($user->timezone)) {
    $timestamp += $user->timezone;
  }
  else {
    $timestamp += variable_get('date_default_timezone', 0);
  }

  // Make sure we apply the site first day of the week setting for dow requests.
  $result = gmdate($format, $timestamp);
  return $result;
}
