<?php

/**
 * @file
 * Administrator interface for Deep Survey module.
 *
 * Admin functions.
 *
 * Admin page related function used purely by the core deep survey module.
 */

/**
 * Displayes a list of the nodes avaliable for management by the current user.
 *
 * @return string
 *   A formatted table listing each survey and some links for each which perform
 *   varous functions.
 */
function deepsurvey_list_surveys() {
  // Get list of node from the database.
  $result = db_query('SELECT n.nid, n.title, u.name, n.created
          FROM {node} n
          INNER JOIN {users} u
          ON u.uid = n.uid
          WHERE n.type = "deepsurvey"');
  while ($line = db_fetch_array($result)) {
    $results[$line['nid']] = $line;
  }

  return theme('deepsurvey_admin_surveys', $results);
}

/**
 * Display information about a survey node and allow items to be added to it.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the survey node.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return string
 *   Formatted age displaying the items in the survey and with controls for
 *   doing various things associated with each.
 */
function deepsurvey_edit_survey($deepsurvey_item_nid, $current_path, $last_page) {
  // Add code to determine if the node is a survey related one or not - go to
  // the node edit page if it is not a survey - add new db table to store
  // this info.
  $node_properties = db_fetch_object(
    db_query('SELECT type, title FROM {node} WHERE nid = %d', $deepsurvey_item_nid));

  if (db_fetch_object(db_query('SELECT type FROM
    {deepsurvey_node_registration} WHERE type = "%s"', $node_properties->type))) {
    drupal_set_title($node_properties->title);
    $rc = drupal_get_form('deepsurvey_edit_form', $deepsurvey_item_nid,
      $node_properties->type, $current_path, $last_page);

    $deepsurvey_title = db_fetch_object(
      db_query('SELECT title FROM {node} WHERE nid = %d', $deepsurvey_item_nid));

  }
  else {
    $rc = '<p>' . t('No Surveys') . '</p>';
  };

  return $rc;
}

/**
 * Allow the researcher to download the results of the survey as a CSV file.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the survey node.
 *
 * @return string
 *   Once the file download has been initaited the function returns the output
 *   of deepsurvey_list_surveys.
 */
function deepsurvey_download_data($deepsurvey_item_nid) {

  $header_standard = array();
  $header_numeric = array();
  $header_text = array();
  $first = 0;
  $no_more_data_flag = FALSE;

  // Remove any old files.
  if (file_exists(file_create_path('deepsurvey' . $deepsurvey_item_nid . '.csv'))) {
    unlink('./' . file_create_path('deepsurvey' . $deepsurvey_item_nid . '.csv'));
  }

  if ($outfile = fopen('./' . file_create_path('deepsurvey' .
    $deepsurvey_item_nid . '.csv'), 'a')) {

    // Create header.
    fwrite($outfile, 'Response,');
    $header_standard = _deepsurvey_download_header('standard',
      $outfile, $deepsurvey_item_nid, $first);
    $header_numeric = _deepsurvey_download_header('numeric',
      $outfile, $deepsurvey_item_nid, $first);
    $header_text = _deepsurvey_download_header('text',
      $outfile, $deepsurvey_item_nid, $first);
    fwrite($outfile, chr(10));

    // Write data.
    $deepsurvey_standard_query = db_query('SELECT question_nid, column_code, row_code,
      loops, instance, userid, response
      FROM {deepsurvey_node_data_standard}
      WHERE deepsurvey_nid = %d
      ORDER BY userid, instance, question_nid, column_code, row_code, loops ',
      $deepsurvey_item_nid);
    $deepsurvey_numeric_query = db_query('SELECT question_nid, column_code,
      row_code, loops, instance, userid, response
      FROM {deepsurvey_node_data_numeric}
      WHERE deepsurvey_nid = %d
      ORDER BY userid, instance, question_nid, column_code, row_code, loops ',
      $deepsurvey_item_nid);
    $deepsurvey_text_query = db_query('SELECT question_nid, column_code, row_code,
      loops, instance, userid, response
      FROM {deepsurvey_node_data_text}
      WHERE deepsurvey_nid = %d
      ORDER BY userid, instance, question_nid, column_code, row_code, loops ',
      $deepsurvey_item_nid);

    $deepsurvey_standard = db_fetch_object($deepsurvey_standard_query);
    $deepsurvey_numeric = db_fetch_object($deepsurvey_numeric_query);
    $deepsurvey_text = db_fetch_object($deepsurvey_text_query);

    while (!$no_more_data_flag) {

      // Determine user id.
      if (!(is_null($deepsurvey_standard->userid)
          || ((($deepsurvey_standard->userid > $deepsurvey_numeric->userid) || ($deepsurvey_standard->instance > $deepsurvey_numeric->instance))
              && !is_null($deepsurvey_numeric->userid))
          || ((($deepsurvey_standard->userid > $deepsurvey_text->userid) || ($deepsurvey_standard->instance > $deepsurvey_text->instance))
              && !is_null($deepsurvey_text->userid)))) {
        fwrite($outfile, $deepsurvey_standard->userid . ',');
      }
      elseif (!(is_null($deepsurvey_numeric->userid)
          || ((($deepsurvey_numeric->userid > $deepsurvey_standard->userid) || ($deepsurvey_numeric->instance > $deepsurvey_standard->instance))
              && !is_null($deepsurvey_standard->userid))
          || ((($deepsurvey_numeric->userid > $deepsurvey_text->userid) || ($deepsurvey_numeric->instance > $deepsurvey_text->instance))
              && !is_null($deepsurvey_text->userid)))) {
        fwrite($outfile, $deepsurvey_numeric->userid . ',');
      }
      elseif (!(is_null($deepsurvey_text->userid)
          || ((($deepsurvey_text->userid > $deepsurvey_numeric->userid) || ($deepsurvey_text->instance > $deepsurvey_numeric->instance))
              && !is_null($deepsurvey_numeric->userid))
          || ((($deepsurvey_text->userid > $deepsurvey_standard->userid) || ($deepsurvey_text->instance > $deepsurvey_standard->instance))
              && !is_null($deepsurvey_standard->userid)))) {
        fwrite($outfile, $deepsurvey_text->userid . ',');
      }

      if (is_null($deepsurvey_standard->userid)
          || ((($deepsurvey_standard->userid > $deepsurvey_numeric->userid) || ($deepsurvey_standard->instance > $deepsurvey_numeric->instance))
              && !is_null($deepsurvey_numeric->userid))
          || ((($deepsurvey_standard->userid > $deepsurvey_text->userid) || ($deepsurvey_standard->instance > $deepsurvey_text->instance))
              && !is_null($deepsurvey_text->userid))) {
        foreach ($header_standard as $check) {
          if ($check != $header_standard[0] || count($header_numeric) != 0 || count($header_text) != 0) {
            fwrite($outfile, ',');
          }
        }
      }
      else {
        $deepsurvey_standard = _deepsurvey_download_data('standard', $outfile,
          $deepsurvey_standard_query, $deepsurvey_standard, $header_standard);
        if (count($header_numeric) != 0 || count($header_text) != 0) {
          fwrite($outfile, ',');
        }
      }

      if (is_null($deepsurvey_numeric->userid)
          || ((($deepsurvey_numeric->userid > $deepsurvey_standard->userid) || ($deepsurvey_numeric->instance > $deepsurvey_standard->instance))
              && !is_null($deepsurvey_standard->userid))
          || ((($deepsurvey_numeric->userid > $deepsurvey_text->userid) || ($deepsurvey_numeric->instance > $deepsurvey_text->instance))
              && !is_null($deepsurvey_text->userid))) {
        foreach ($header_numeric as $check) {
          if ($check != $header_numeric[0] || count($header_text) != 0) {
            fwrite($outfile, ',');
          }
        }
      }
      else {
        $deepsurvey_numeric = _deepsurvey_download_data('numeric', $outfile,
          $deepsurvey_numeric_query, $deepsurvey_numeric, $header_numeric);
        if (count($header_text) != 0) {
          fwrite($outfile, ',');
        }
      }

      if (is_null($deepsurvey_text->userid)
          || ((($deepsurvey_text->userid > $deepsurvey_numeric->userid) || ($deepsurvey_text->instance > $deepsurvey_numeric->instance))
              && !is_null($deepsurvey_numeric->userid))
          || ((($deepsurvey_text->userid > $deepsurvey_standard->userid) || ($deepsurvey_text->instance > $deepsurvey_standard->instance))
              && !is_null($deepsurvey_standard->userid))) {
        foreach ($header_text as $check) {
          if ($check != $header_text[0]) {
            fwrite($outfile, ',');
          }
        }
      }
      else {
        $deepsurvey_text = _deepsurvey_download_data('text', $outfile,
          $deepsurvey_text_query, $deepsurvey_text, $header_text);
      }

      if (is_null($deepsurvey_standard->userid) && is_null($deepsurvey_numeric->userid) && is_null($deepsurvey_text->userid)) {
        $no_more_data_flag = TRUE;
      }
      else {
        fwrite($outfile, chr(10));
      }
    }

    if (fclose($outfile)) {
      file_transfer('./' .
        file_create_path('deepsurvey' . $deepsurvey_item_nid . '.csv'), array(
          'Content-Type: text/csv',
          'Content-Disposition: attachment; filename=deepsurvey' .
          $deepsurvey_item_nid . '.csv',
        ));
    }
    else {
      drupal_set_message(t('error creating data file'), 'error');
    }

  }
  else {
    drupal_set_message(t('error creating data file'), 'error');
  }

  return deepsurvey_list_surveys();
}

/**
 * Clears all survey data.
 *
 * Clear all data from the survey - this will eventaully be hidden from
 * unprivledged researchers.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the survey node.
 *
 * @return string
 *   Once the data has been cleared the function returns the output of
 *   deepsurvey_list_surveys.
 */
function deepsurvey_clear_data($deepsurvey_item_nid) {
  // Delete old values.
  db_query('DELETE FROM {deepsurvey_node_data_numeric}
    WHERE deepsurvey_nid = %d', $deepsurvey_item_nid);
  db_query('DELETE FROM {deepsurvey_node_data_text}
    WHERE deepsurvey_nid = %d', $deepsurvey_item_nid);
  db_query('DELETE FROM {deepsurvey_node_data_standard}
    WHERE deepsurvey_nid = %d', $deepsurvey_item_nid);
  db_query('DELETE FROM {deepsurvey_node_state}
    WHERE deepsurvey_nid = %d', $deepsurvey_item_nid);
  db_query('DELETE FROM {deepsurvey_node_state_global}
    WHERE deepsurvey_nid = %d', $deepsurvey_item_nid);
  db_query('DELETE FROM {deepsurvey_node_order}
    WHERE deepsurvey_nid = %d', $deepsurvey_item_nid);
  db_query('DELETE FROM {deepsurvey_node_userid_map}
    WHERE deepsurvey_nid = %d', $deepsurvey_item_nid);

  // Remove from other types.
  $result = db_query('SELECT type FROM {deepsurvey_node_registration}
    WHERE container = 1');
  while ($container_node = db_fetch_array($result)) {
    db_query('DELETE FROM {%s_node_state}
      WHERE deepsurvey_nid = %d', $container_node->type, $node->nid);
  }

  drupal_set_message(t('All data deleted from survey @name',
    array('@name' => check_plain($deepsurvey_item_nid))));
  return deepsurvey_list_surveys();
}

/**
 * Add existing node to a survey (or similar).
 *
 * Calles the form function to create the form for adding exising nodes to the
 * survey.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the survey node.
 *
 * @param int $order
 *   The order of the item before the point where the existing items are to be
 *   added into the survey, 0 if they are to be added to the beginning of the
 *   survey.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return string
 *   Form.
 */
function deepsurvey_edit_add_existing_item($deepsurvey_item_nid, $order, $current_path, $last_page) {
  $rc = drupal_get_form('deepsurvey_add_existing_form', $deepsurvey_item_nid,
    $order, $current_path, $last_page);

  return $rc;
}

/**
 * Add a new node to a survey (or similar).
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the survey node.
 *
 * @param int $order
 *   The order of the item before where the existing items are to be added into
 *   the survey, 0 if they are to be added to the beginning of the survey.
 *
 * @param string $current_path
 *   The current path to this page, used to build links to the child nodes.
 *
 * @param string $last_page
 *   The path to the previous path for redirects, needed because of the
 *   limitations in drupal menu paths.
 *
 * @return string
 *   Form.
 */
function deepsurvey_edit_add_new_item($deepsurvey_item_nid, $order, $current_path, $last_page) {
  $rc = drupal_get_form('deepsurvey_add_new_form', $deepsurvey_item_nid,
    $order, $current_path, $last_page);

  return $rc;
}

// DOWNLOAD FUNCTIONS.
/**
 * Writes the download file header.
 *
 * Called from the download_data function to write the header on the first line
 * of the csv file.
 *
 * @param string $table_suffix
 *   The suffix of the table contain the data (there are three data tables - on
 *   for boolean data, one for text data and one for numeric data).
 *
 * @param object $download_file_handle
 *   The file handle of the csv file.
 *
 * @param int $deepsurvey_item_nid
 *   The node id of the survey node.
 *
 * @param int $first
 *   Indicates if this is the first time this function has been called
 *   (0 is yes, 1 is no).
 *
 * @return array
 *   Array of the csv file header.
 */
function _deepsurvey_download_header($table_suffix, $download_file_handle, $deepsurvey_item_nid, &$first) {
  $header = array();

  $deepsurvey_data_head = db_query('
    SELECT DISTINCT question_nid, column_code, row_code, loops
    FROM {deepsurvey_node_data_%s}
    WHERE deepsurvey_nid = %d
    ORDER BY question_nid, column_code, row_code, loops',
    $table_suffix, $deepsurvey_item_nid);
  while ($current_data_head = db_fetch_object($deepsurvey_data_head)) {
    if ($first == 0) {
      $first = 1;
    }
    else {
      fwrite($download_file_handle, ',');
    }
    $var = $current_data_head->question_nid . $current_data_head->column_code .
      $current_data_head->row_code . $current_data_head->loops;
    $q_title = db_fetch_object(db_query(db_rewrite_sql('
      SELECT title FROM {node}
      WHERE nid = %d'), $current_data_head->question_nid));
    $header[] = $var;
    $output_field_name = $q_title->title;
    if ($current_data_head->column_code) {
      $output_field_name = $output_field_name . '_' . $current_data_head->column_code;
    }
    if ($current_data_head->row_code) {
      $output_field_name = $output_field_name . '_' . $current_data_head->row_code;
    }
    if ($current_data_head->loops) {
      $output_field_name = $output_field_name . '_' . $current_data_head->loops;
    }
    fwrite($download_file_handle, $output_field_name);
  }

  return $header;
}

/**
 * Called from the download_data function to write data to the csv file.
 *
 * @param sting $table_suffix
 *   The suffix of the table contain the data (there are three data tables - on
 *   for boolean data, one for text data and one for numeric data).
 *
 * @param object $download_file_handle
 *   The file handle of the csv file.
 *
 * @param object $current_data_query
 *   The data from the db_query call.
 *
 * @param object $current_data
 *   Return value from the last db_fetch_object call.
 *
 * @param array $header
 *   An array of all the variables in the csv header.
 *
 * @return object
 *   Next set of data to process or null if no more.
 */
function _deepsurvey_download_data($table_suffix, $download_file_handle, &$current_data_query, $current_data, $header) {
  $current_user = $current_data->userid;
  $current_instance = $current_data->instance;
  $var_counter = 0;
  $var_counter_in = 0;
  $first = TRUE;

  while (!is_null($current_data->userid) && $current_user == $current_data->userid && $current_instance == $current_data->instance) {

    if ($first == TRUE) {
      $first = FALSE;
    }
    else {
      fwrite($download_file_handle, ',');
    }

    while ($header[$var_counter + $var_counter_in] != $current_data->question_nid . $current_data->column_code . $current_data->row_code . $current_data->loops && ($var_counter + $var_counter_in) < count($header)) {
      fwrite($download_file_handle, ',');
      $var_counter_in++;
    }
    if ($current_data_body->response === 0) {
      fwrite($download_file_handle, 0);
    }
    else {
      fwrite($download_file_handle, $current_data->response);
    }
    $var_counter++;
    $current_data = db_fetch_object($current_data_query);
  }

  return $current_data;

}

// THEME FUNCTIONS
// Remember to updated deepsurvey_theme() in survey.module
/**
 * Theme the admin deep survey table.
 *
 * @param string $results
 *   As returned by query in calling function.
 *
 * @ingroup themeable
 */
function theme_deepsurvey_admin_surveys($results) {
  $output = '';
  $rows = array();

  if ($results != NULL) {
    while (list($key, $result) = each($results)) {
      $rows[] = array(
        l($result['title'], 'admin/deepsurvey/' . $result['nid']),
        check_plain($result['name']),
        format_date($result['created'], 'small'),
        l(t('Download'), 'admin/deepsurvey/' . $result['nid'] . '/download'),
        l(t('Clear'), 'admin/deepsurvey/' . $result['nid'] . '/clear'),
      );
    }
  }

  $header = array(
    t('title'),
    t('Created by'),
    t('Created on'),
    t('Download data'),
    t('Clear data'),
  );

  if (!empty($rows)) {
    $output .= theme('table', $header, $rows);
  }
  else {
    $output .= t('No surveys found.');
  }
  return $output;
}


/**
 * Theme the form for adding questions to a survey.
 */
function theme_deepsurvey_add_existing_questions_table($form) {

  $headers = array(t('Name'), t('Date'), t('Select'));
  $rows = array();

  if (!empty($form)) {
    foreach (element_children($form) as $nid) {

      $rows[] = array(
        'data' => array(
          check_plain(drupal_render($form[$nid]['name'])),
          format_date(drupal_render($form[$nid]['created']), 'small'),
          drupal_render($form[$nid]['select box']),
        ),
      );
    }

  }
  $table = theme('table', $headers, $rows);
  return $table . drupal_render($form);
}

/**
 * Theme the form for editing the survey.
 */
function theme_deepsurvey_edit_table($form) {

  $headers = array(t('Order'), t('Id'), t('Name'), t('Link'), t('Reorder'),
    t('Remove'), t('Add new'), t('Add existing'));
  $rows = array();

  if (!empty($form)) {
    foreach (element_children($form) as $nid) {

      $rows[] = array(
        'data' => array(
          check_plain(drupal_render($form[$nid]['order'])),
          drupal_render($form[$nid]['iid']),
          drupal_render($form[$nid]['name']),
          drupal_render($form[$nid]['link']),
          drupal_render($form[$nid]['reorder box']),
          drupal_render($form[$nid]['remove box']),
          drupal_render($form[$nid]['add new']),
          drupal_render($form[$nid]['add existing']),
        ),
      );
    }

  }
  $table = theme('table', $headers, $rows);
  return $table . drupal_render($form);
}
